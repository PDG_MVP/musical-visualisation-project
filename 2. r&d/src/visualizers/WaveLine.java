package visualizers;

import java.awt.*;

public class WaveLine {

    // Noise : determines how "noisy" the cubic curves will be.
    //         -> must be a multiple of 4
    //         -> the lower the integer, the more sharp curves the lines will have
    //         -> Adding a smooth factor will add a line
    private int noise;
    private Color color;
    private Stroke stroke;
    private int randFactor;

    public WaveLine(int noise, Color color, Stroke stroke, int randFactor) {
        this.noise = noise;
        this.color = color;
        this.stroke = stroke;
        this.randFactor = randFactor;
    }

    public int getNoise() {
        return noise;
    }

    public void setNoise(int noise) {
        this.noise = noise;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Stroke getStroke() {
        return stroke;
    }

    public void setStroke(Stroke stroke) {
        this.stroke = stroke;
    }

    public int getRandFactor() {
        return randFactor;
    }

    public void setRandFactor(int randFactor) {
        this.randFactor = randFactor;
    }
}
