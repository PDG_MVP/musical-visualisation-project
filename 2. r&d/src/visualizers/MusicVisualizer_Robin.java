package visualizers;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.File;
import java.util.ArrayList;

/**
 * Project : FennecWave - Music Visualization Project
 * Author  : Robin Reuteler
 * Date    : 6.10.2020
 *
 * This class is used for research purposes on the
 * conversion from an MP3 file to a PNG file
 */

public class MusicVisualizer_Robin implements MusicVisualizer {

    private static final int SECTION_SIZE = 60*44100;//1 minute of audio

    @Override
    public void generateRepresentation(File audioFile, Filter filter, String outputPath, int width) {
        try
        {
            AudioInputStream sourceAudio = AudioSystem.getAudioInputStream(audioFile);
            AudioFormat format = sourceAudio.getFormat();
            long noOfFrames = sourceAudio.getFrameLength();//Total number of samples of the audioStream
            int sizeOfSample = format.getSampleSizeInBits();
            int sizeOfFrame = format.getFrameSize();
            //TODO : Average out multiple samples to reduce data size
            int[] samples = new int[SECTION_SIZE];

            System.out.println(format.getEncoding().toString());
            if(format.getEncoding() == AudioFormat.Encoding.PCM_SIGNED)
            {
                int i=0;
                byte[] sampleFrame = new byte[format.getChannels()*sizeOfSample/8];
                int[] channels = new int[format.getChannels()];
                //While there are audio bytes to be read
                while(sourceAudio.read(sampleFrame, 0, sizeOfFrame)> 0 && i<samples.length)
                {
                    //DEBUG
                    for(int j=0; j<sizeOfFrame; ++j)
                        if(sampleFrame[j] != 0) {
                            System.out.println("Found sound : "+sampleFrame[j]);
                        }

                    for(int channelNo=0; channelNo<format.getChannels(); ++channelNo)
                    {
                        channels[channelNo] = 0;
                        for(int byteNo=0; byteNo<sizeOfSample/8; ++byteNo)
                        {
                            if(format.isBigEndian())
                            {
                                channels[channelNo] += sampleFrame[channelNo*sizeOfSample/8+byteNo] << ((sizeOfSample / 8 - byteNo)*8);
                            }
                            else
                            {
                                channels[channelNo] += sampleFrame[channelNo*sizeOfSample/8+byteNo] << byteNo*8;
                            }
                        }
                    }
                    int channelAvg = 0;
                    for(int sampleChannel : channels)
                    {
                        channelAvg += sampleChannel;
                    }
                    channelAvg/=format.getChannels();
                    samples[i]=channelAvg;
                    ++i;
                }

                //DEBUG
                if(sourceAudio.available()/sizeOfSample>0 || i<noOfFrames)
                {
                    System.out.println("Warning : The file ended unexpectedly");
                }
            }

        }
        catch(Exception e)
        {

        }
    }
}
