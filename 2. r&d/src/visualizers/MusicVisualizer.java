package visualizers;

import java.io.File;

public interface MusicVisualizer {

    void generateRepresentation(File mp3File, Filter filter, String outputPath, int width);

}
