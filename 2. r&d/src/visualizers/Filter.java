package visualizers;

import java.util.ArrayList;

public class Filter {

    private String name;
    private ArrayList<WaveLine> wavelines;

    public Filter(String name, ArrayList<WaveLine> wavelines) {
        this.name = name;
        this.wavelines = wavelines;
    }

    public String getName() {
        return name;
    }

    public ArrayList<WaveLine> getWavelines() {
        return wavelines;
    }
}
