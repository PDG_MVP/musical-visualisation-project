package visualizers;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Filters {

    public static Filter debugFilter = new Filter(
            "Debug",
            new ArrayList<>(Arrays.asList(
                    new WaveLine(4, new Color(255, 255, 255, 255),
                            new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0)))
    );

    public static Filter basicFilter = new Filter(
            "Basic",
            new ArrayList<>(Arrays.asList(
                    new WaveLine(32, new Color(61, 177, 255, 255),
                            new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0),
                    new WaveLine(16, new Color(101, 217, 208, 255),
                            new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0),
                    new WaveLine(4, new Color(61, 255, 170, 255),
                            new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0)
            ))
    );

    public static Filter minimalistFilter = new Filter(
            "Minimalist",
            new ArrayList<>(Arrays.asList(
                    new WaveLine(16, new Color(255, 255, 255, 255),
                            new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 9f, new float[] {9f}, 0f), 0)
            ))
    );

    public static Filter highContrastFilter = new Filter(
            "High Contrast",
            new ArrayList<>(Arrays.asList(
                    new WaveLine(16, new Color(255, 255, 255, 126),
                            new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0),
                    new WaveLine(4, new Color(255, 255, 255, 255),
                            new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0)
            ))
    );

    public static Filter lightSunshineFilter = new Filter(
            "Light Sunshine",
            new ArrayList<>(Arrays.asList(
                    new WaveLine(32, new Color(255, 151, 20, 65),
                            new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0),
                    new WaveLine(16, new Color(255, 177, 49, 122),
                            new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0),
                    new WaveLine(4, new Color(255, 221, 0, 255),
                            new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0)
            ))
    );

    public static Filter jungleNeon = new Filter(
            "Jungle Neon",
            new ArrayList<>(Arrays.asList(
                    new WaveLine(4, new Color(126, 198, 26, 100),
                            new BasicStroke(20, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0),
                    new WaveLine(4, new Color(173, 255, 57, 255),
                            new BasicStroke(15, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0),
                    new WaveLine(4, new Color(197, 255, 197, 255),
                            new BasicStroke(9, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 0)
            ))
    );

    public static Filter synthwave = new Filter(
            "Synthwave",
            new ArrayList<>(Arrays.asList(
                    new WaveLine(4, new Color(217, 0, 255, 255),
                            new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), -10),
                    new WaveLine(4, new Color(0, 255, 196, 255),
                            new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 10),
                    new WaveLine(4, new Color(255, 230, 0, 255),
                            new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 1)
            ))
    );

    public static Filter threeDee = new Filter(
            "ThreeDee",
            new ArrayList<>(Arrays.asList(
                    new WaveLine(4, new Color(0, 136, 255, 150),
                            new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), -10),
                    new WaveLine(4, new Color(255, 0, 0, 150),
                            new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), 10)
            ))
    );
}
