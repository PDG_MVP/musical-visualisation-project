package visualizers;

import java.io.File;

/**
 * Project : FennecWave - Music Visualization Project
 * Author  : Claire Delhomme
 * Date    : 6.10.2020
 *
 * This class is used for research purposes on the
 * conversion from an MP3 file to a PNG file
 */

public class MusicVisualizer_Claire implements MusicVisualizer {
    @Override
    public void generateRepresentation(File mp3File, Filter filter, String outputPath, int width) {
    }
}
