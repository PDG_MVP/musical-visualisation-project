import visualizers.*;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static final String outputPath = "output/";

    public static void main(String[] args) {

        // Test all music visualisation algorithms
        ArrayList<MusicVisualizer> visualizers = new ArrayList<>(Arrays.asList(
                new visualizers.MusicVisualizer_Steph()
                //new visualizers.MusicVisualizer_Claire(),
                //new MusicVisualizer_Robin()
        ));

        // Available filters
        ArrayList<Filter> filters = new ArrayList<>(Arrays.asList(
                Filters.basicFilter/*,
                Filters.minimalistFilter,
                Filters.highContrastFilter,
                Filters.lightSunshineFilter*/
        ));

        for(MusicVisualizer visualizer : visualizers) {
            File dir = new File("input");
            File[] directoryListing = dir.listFiles();
            if (directoryListing != null) {
                for(Filter filter : filters) {
                    String outputFilePath = outputPath + filter.getName();
                    new File(outputFilePath).mkdir();

                    for (File child : directoryListing) {
                        if(child.getName().contains("wav"))
                            visualizer.generateRepresentation(child, filter, outputFilePath, 4000);
                    }
                }
            }
        }

    }

}
