package com.fennecwave.fwapi;

import com.fennecwave.fwapi.model.User;
import com.fennecwave.fwapi.model.Visual;
import com.fennecwave.fwapi.repository.UserRepository;
import com.fennecwave.fwapi.repository.VisualRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class VisualEntityRepositoryIT {
    private static int n = 0;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private VisualRepository visualRepository;

    @Autowired
    private UserRepository userRepository;

    private final Date now = new Date();

    // Check if the database is running and the context can load
    @Test
    void contextLoads() {
    }

    @Test
    void injectedComponentsAreNotNull() {
        assertThat(dataSource).isNotNull();
        assertThat(jdbcTemplate).isNotNull();
        assertThat(entityManager).isNotNull();
        assertThat(visualRepository).isNotNull();
        assertThat(userRepository).isNotNull();
    }

    @Test
    void canInsertVisualInDatabase() {
        Visual v = createAndSaveVisual();
        Long id = v.getId_visual();
        assertThat(visualRepository.findById(id).isPresent());
        assertThat(visualRepository.findById(id).get().getId_visual()).isEqualTo(id);
      }

    @Test
    void storedInDBWithCorrectAttributes() {
        Visual v = createAndSaveVisual();
        Long id = v.getId_visual();
        assertThat(visualRepository.findById(id).isPresent());
        Visual foundVisual = visualRepository.findById(id).get();

        assertThat(foundVisual.getId_visual()).isEqualTo(id);
        assertThat(foundVisual.getTitle()).isEqualTo(v.getTitle());
        assertThat(foundVisual.getSubtitle()).isEqualTo(v.getSubtitle());
        assertThat(foundVisual.getVisualimg()).isEqualTo(v.getVisualimg());
        assertThat(foundVisual.getAuthor()).isEqualTo(v.getAuthor());
        assertThat(foundVisual.getCreationDate()).isEqualTo(v.getCreationDate());
        assertThat(foundVisual.getModificationDate()).isEqualTo(v.getModificationDate());
        assertThat(foundVisual.getLength()).isEqualTo(v.getLength());


        //TODO cleanup
       // assertThat(foundVisual.getLengthseconds()).isNotEqualTo(0);

        //should fail?? avoid equals
       // assertThat(!visualRepository.findById(id).get().getIdvisual().equals(id));
        //Fail ok
        //assertThat(visualRepository.findById(id).get().getIdvisual()).isNotEqualTo(id);
    }

      @Test
      void allFindMethodsFromVisualRepo(){
          Visual v = createAndSaveVisual();
          Long id = v.getId_visual();
          String authorName = v.getAuthor();
          String title = v.getTitle();
          String subtitle = v.getSubtitle();

          assertThat(visualRepository.findById(id).isPresent());
          Visual foundVisual = visualRepository.findById(id).get();

          assertThat(visualRepository.findByAuthor(authorName).isPresent());
          assertThat(visualRepository.findByAuthor(authorName).get().get(0).getId_visual().equals(id));

          assertThat(visualRepository.findByTitle(title).isPresent());
          assertThat(visualRepository.findByTitle(title).get().get(0).getId_visual().equals(id));

          assertThat(visualRepository.findBySubtitle(subtitle).isPresent());
          assertThat(visualRepository.findBySubtitle(subtitle).get().get(0).getId_visual().equals(id));

      }

    @Test
    public void whenDeleteAllFromRepository_thenRepositoryShouldBeEmpty() {
        visualRepository.deleteAll();
        assertThat(visualRepository.count()).isEqualTo(0);
    }

    private String saveUserToDb(){
        //Create author
        User u = new User();
        u.setUsername("John");
        u.setPassword("1234");
        // Save to database
        userRepository.save(u);
        return u.getUsername();
    }

    private Visual createAndSaveVisual(){
        // Create visual
        String title = "Title" + n;
        String subtitle = "Subtitle" + n;
        Visual v = new Visual();
        v.setTitle(title);
        v.setSubtitle(subtitle);
        v.setVisualimg("visualimg");
        v.setCreationDate(now);
        v.setModificationDate(now);
        v.setLength("23:34");
        v.setAuthor(saveUserToDb());
        //Save to database
        visualRepository.save(v);
        n++;
        return v;
    }
}
