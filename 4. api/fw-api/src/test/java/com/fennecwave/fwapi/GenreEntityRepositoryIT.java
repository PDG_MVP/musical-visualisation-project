package com.fennecwave.fwapi;

import com.fennecwave.fwapi.repository.GenreRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
class GenreEntityRepositoryIT {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private GenreRepository genreRepository;

	// Check if the database is running and the context can load
	@Test
	void contextLoads() {
	}

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(dataSource).isNotNull();
		assertThat(jdbcTemplate).isNotNull();
		assertThat(entityManager).isNotNull();
		assertThat(genreRepository).isNotNull();
	}

	// TODO : Test that they are in alphabetical order

	// TODO : Test that doubles are erased

	// Genres are created only during the pre-population phase of the database
	/*@Test
	void cannotInsertGenreInDatabase() {

		// Save to database
		genreRepository.save(new Genre("my_own_special_genre_that_no_one_understands"));
	}*/

	@Test
	public void whenDeleteAllFromRepository_thenRepositoryShouldBeEmpty() {
		genreRepository.deleteAll();
		assertThat(genreRepository.count()).isEqualTo(0);
	}
}
