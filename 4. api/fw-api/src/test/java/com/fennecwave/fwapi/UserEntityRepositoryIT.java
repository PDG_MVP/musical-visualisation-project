package com.fennecwave.fwapi;

import com.fennecwave.fwapi.model.User;
import com.fennecwave.fwapi.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
class UserEntityRepositoryIT {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private UserRepository userRepository;

	// Check if the database is running and the context can load
	@Test
	void contextLoads() {
	}

	@Test
	void injectedComponentsAreNotNull() {
		assertThat(dataSource).isNotNull();
		assertThat(jdbcTemplate).isNotNull();
		assertThat(entityManager).isNotNull();
		assertThat(userRepository).isNotNull();
	}

	@Test
	void canCreateUser() {
		// Create user
		User u = new User();
		u.setUsername("John");
		u.setPassword("1234");
		// Save to database
		userRepository.save(u);

		assertThat(userRepository.findByUsername("John").isPresent()).isTrue();
		assertThat(userRepository.findByUsername("John").get()).isNotNull();
		assertThat(userRepository.findByUsername("John").get().getUsername()).isEqualTo("John");
	}

	@Test
	void canInsertUserOnlyIfUniqueUsername() {
		//TODO
		assertThat(true).isTrue();
	}

	@Test
	public void canFindUserById() {
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();

		entityManager.persist(user1);
		entityManager.persist(user2);
		entityManager.persist(user3);

		User foundUser = userRepository.findById(user2.getId()).get();

		assertThat(foundUser).isEqualTo(user2);
	}

	@Test
	public void whenDeleteAllFromRepository_thenRepositoryShouldBeEmpty() {
		userRepository.deleteAll();
		assertThat(userRepository.count()).isEqualTo(0);
	}
}
