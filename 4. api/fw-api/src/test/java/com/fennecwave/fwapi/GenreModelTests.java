package com.fennecwave.fwapi;

import com.fennecwave.fwapi.model.Genre;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class GenreModelTests {
    private final String label = "Experimental";

    @Test
    public void canCreateAGenreWithALabel() {
        Genre newGenre = new Genre(label);

        assertEquals(label, newGenre.getLabel());
    }

    @Test
    public void genreEquals() {
        Genre newGenre = new Genre(label);
        Genre sameGenre = new Genre(label);
        Genre otherGenre = new Genre(label + " New Wave");

        assertEquals(newGenre, sameGenre);
        assertNotEquals(newGenre, otherGenre);
    }

}
