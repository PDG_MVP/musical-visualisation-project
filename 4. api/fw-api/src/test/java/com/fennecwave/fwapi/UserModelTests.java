package com.fennecwave.fwapi;

import com.fennecwave.fwapi.model.User;
import com.fennecwave.fwapi.model.UserVisualLike;
import com.fennecwave.fwapi.model.Visual;
import org.junit.jupiter.api.Test;
import org.tritonus.share.ArraySet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class UserModelTests {
    private final Long id = 1L;
    private final String username = "John";
    private final String password = "1234";
    private final List<UserVisualLike> likes = new ArrayList<>();

    @Test
    public void canCreateAUser() {
        User newUser = new User(id, username, password, likes);

        assertEquals(id, newUser.getId());
        assertEquals(username, newUser.getUsername());
        assertEquals(password, newUser.getPassword());
    }

    @Test
    public void userEquals() {
        User newUser = new User(id, username, password, likes);
        User sameUser  = new User(id, username, password, likes);
        User otherUser = new User(id + 1, username, password, likes);

        assertEquals(newUser, sameUser);
        assertNotEquals(newUser, otherUser);
    }

}
