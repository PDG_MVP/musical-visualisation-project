package com.fennecwave.fwapi;

import com.fennecwave.fwapi.model.Genre;
import com.fennecwave.fwapi.model.User;
import com.fennecwave.fwapi.model.UserVisualLike;
import com.fennecwave.fwapi.model.Visual;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class VisualModelTests {
    private final Long id = 1L;
    private final String title = "Hotel California";
    private final String subtitle = "Eagles";
    private final String filename = "hotel_california.wav";
    private final String visualimg = "hotel_california.png";
    private final String length = "35:34";
    private final String authorName = "John";

    private final Date now = new Date();

    private final List<Genre> genres = new ArrayList<>();
    private final List<UserVisualLike> likes = new ArrayList<>();

    private final String pathServed = "http://api.fennecwave.com/dummy";

    @Test
    public void canCreateAVisual() {
        Visual visual = new Visual(id, title, subtitle, visualimg, filename, now, now, length, authorName, pathServed, genres, likes);

        assertEquals(id, visual.getId_visual());
        assertEquals(title, visual.getTitle());
        assertEquals(subtitle, visual.getSubtitle());
        assertEquals(visualimg, visual.getVisualimg());
        assertEquals(now, visual.getCreationDate());
        assertEquals(now, visual.getModificationDate());
        assertEquals(authorName, visual.getAuthor());
    }

    @Test
    public void visualEquals() {
        Visual visual = new Visual(id, title, subtitle,visualimg, filename, now, now, length, authorName, pathServed, genres, likes);
        Visual sameVisual = new Visual(id, title, subtitle,visualimg, filename, now, now, length, authorName, pathServed, genres, likes);
        Visual otherVisual = new Visual(id + 1, title, subtitle,visualimg, filename, now, now, length, authorName, pathServed, genres, likes);

        assertEquals(visual, sameVisual);
        assertNotEquals(visual, otherVisual);
    }

}
