package com.fennecwave.fwapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class UserVisualLikeKey implements Serializable {

    @Column(name = "user_id")
    Long userId;

    @Column(name = "visual_id")
    Long visualId;
}
