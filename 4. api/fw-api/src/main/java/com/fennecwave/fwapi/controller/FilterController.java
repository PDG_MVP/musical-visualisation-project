package com.fennecwave.fwapi.controller;

import com.fennecwave.fwapi.repository.FilterRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping({"/filters"})
public class FilterController {

    private final FilterRepository repository;

    FilterController(FilterRepository filterRepository) {
        this.repository = filterRepository;
    }

    // Find all genres
    @GetMapping
    public List findAll(){
        return repository.findAll();
    }

}
