package com.fennecwave.fwapi.controller;

import com.fennecwave.fwapi.model.User;
import com.fennecwave.fwapi.model.UserVisualLike;
import com.fennecwave.fwapi.model.Visual;
import com.fennecwave.fwapi.repository.UserRepository;
import com.fennecwave.fwapi.repository.VisualRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.*;

@RestController
@RequestMapping({"/visuals"})
public class VisualController {
    private final VisualRepository visualRepository;
    private final UserRepository userRepository;

    VisualController(VisualRepository visualRepository, UserRepository userRepository){
        this.visualRepository = visualRepository;
        this.userRepository = userRepository;
    }

    //Find all visuals
    @GetMapping
    public List findAll(){
        return visualRepository.getAllAuthoredVisuals();
    }

    // Get visual by idvisual
    @GetMapping(path = {"/{idvisual}"})
    public ResponseEntity<Visual> findById(@PathVariable long idvisual){
        return visualRepository.findById(idvisual)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    // Get visual by title
    @GetMapping(path = {"/title/{title}"})
    public ResponseEntity<List<Visual>> findByTitle(@PathVariable String title){
        return visualRepository.findByTitle(title)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    // Get visual by subtitle (artist)
    @GetMapping(path = {"/subtitle/{subtitle}"})
    public ResponseEntity<List<Visual>> findBySubtitle(@PathVariable String subtitle){
        return visualRepository.findBySubtitle(subtitle)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    // Get visual by author id
    @GetMapping(path = {"/author/{author}"})
    public List<Visual> findByAuthor(@PathVariable String author) {

        Optional<List<Visual>> visualsFromAuthor = visualRepository.findByAuthor(author);
        if(visualsFromAuthor.isEmpty())
            return null;

        return visualsFromAuthor.get();
    }

    // Get most trending visuals
    @GetMapping(path = {"/trends"})
    public List<Visual> findByTrends(){
        return visualRepository.findByTrends();
    }

    // Get visual by favourites from an author
    @GetMapping(path = {"/favourites/{author}"})
    public List<Visual> findByAuthorFavourites(@PathVariable String author) {
        Optional<User> user = userRepository.findByUsername(author);

        if(user.isEmpty())
            return new ArrayList<>();

        List<Visual> favourites = new ArrayList<>();
        for(UserVisualLike like : user.get().getLikes())
            favourites.add(like.getVisual());

        return favourites;
    }

    // Get all visuals that have been favourited by others from an author
    @GetMapping(path = {"/favourited/{author}"})
    public List<Visual> findByAuthorFavourited(@PathVariable String author) {
        Optional<User> user = userRepository.findByUsername(author);

        if(user.isEmpty())
            return new ArrayList<>();

        return visualRepository.getAllFavouritedVisuals(user.get().getUsername());
    }


    // Create a new visual
    @PostMapping
    public Visual create(@RequestBody Visual visual) {
        return visualRepository.save(visual);
    }

    // Update a visual
    @PutMapping(value= "/{idvisual}")
    public ResponseEntity<Visual> update(@PathVariable("idvisual") long id,
                                       @RequestBody Visual visual){
        Date now = new Date();
        return visualRepository.findById(id)
                .map(record -> {
                    record.setTitle(visual.getTitle());
                    record.setSubtitle(visual.getSubtitle());
                    record.setModificationDate(now);
                    Visual updated = visualRepository.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    // Delete a visual
    @DeleteMapping(path ={"/{idvisual}"})
    public ResponseEntity<?> delete(@PathVariable("idvisual") long id) {
        return visualRepository.findById(id)
                .map(record -> {
                    visualRepository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }

}
