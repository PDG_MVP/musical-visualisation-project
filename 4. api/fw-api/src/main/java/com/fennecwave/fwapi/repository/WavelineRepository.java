package com.fennecwave.fwapi.repository;

import com.fennecwave.fwapi.model.Waveline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WavelineRepository extends JpaRepository<Waveline, Long> {
}
