package com.fennecwave.fwapi.seeder;

import com.fennecwave.fwapi.model.Filter;
import com.fennecwave.fwapi.model.Genre;
import com.fennecwave.fwapi.model.User;
import com.fennecwave.fwapi.model.Waveline;
import com.fennecwave.fwapi.repository.*;
import com.fennecwave.fwapi.service.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import static com.fennecwave.fwapi.MusicVisualizerConverter.DASHED_LINE;
import static com.fennecwave.fwapi.MusicVisualizerConverter.STRAIGHT_LINE;

@Component
public class DatabaseSeeder {
        private StorageService storageService;
        private UserRepository userRepository;
        private GenreRepository genreRepository;
        private VisualRepository visualRepository;
        private WavelineRepository wavelineRepository;
        private FilterRepository filterRepository;

    @Autowired
    public DatabaseSeeder(StorageService storageService, UserRepository userRepository, GenreRepository genreRepository, VisualRepository visualRepository, WavelineRepository wavelineRepository, FilterRepository filterRepository) {
        this.storageService = storageService;
        this.userRepository = userRepository;
        this.genreRepository = genreRepository;
        this.visualRepository = visualRepository;
        this.wavelineRepository = wavelineRepository;
        this.filterRepository = filterRepository;
    }


    @EventListener
    public void seed(ContextRefreshedEvent event) { //Warning: order is important (at least until i get cascadeType right)
        seed_storage();
        seed_users();
        seed_filters();
        seed_visuals();
        seed_genres();
    }

	private void seed_storage() {
			storageService.init();
	}

    private void seed_users() {
            userRepository.deleteAll();
			User dev = new User();
			dev.setUsername("FennecwaveDev");
			dev.setPassword("1234");
			if(userRepository.findByUsername("FennecwaveDev").isEmpty()) {
                userRepository.save(dev);
            }
    }

    private void seed_genres() {
        genreRepository.deleteAll();

        // Resource file of all genres (cleaned and organized)
        InputStream genres = null;
        try {
            genres = new ClassPathResource("static/genres.txt").getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(genres, StandardCharsets.UTF_8));

			// Save each genres separately from the reader
			String genre;
			while ((genre = br.readLine()) != null) {
				genreRepository.save(new Genre(genre));
			}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void seed_visuals(){
        visualRepository.deleteAll();
    }

    private void seed_filters(){
        // Clear all filters (first wavelines then filter)
        wavelineRepository.deleteAll();
        filterRepository.deleteAll();

        // Basic
        saveNewFilter(filterRepository, wavelineRepository,
        "Basic", new ArrayList<>(Arrays.asList(
                new Waveline(32, "#43b1ff", 0.8f, STRAIGHT_LINE, 5, 0),
                new Waveline(16, "#65d9cf", 0.9f, STRAIGHT_LINE, 2, 0),
                new Waveline(4, "#3dffab", 1, STRAIGHT_LINE, 1, 0)
        )));

        // Blueprint
        saveNewFilter(filterRepository, wavelineRepository,
        "Blueprint", new ArrayList<>(Arrays.asList(
                new Waveline(32, "#00d0ff", 0.4f, DASHED_LINE, 2, 0),
                new Waveline(4, "#00d0ff", 1, DASHED_LINE, 1, 0)
        )));

        // Firey
        saveNewFilter(filterRepository, wavelineRepository,
        "Firey", new ArrayList<>(Arrays.asList(
                new Waveline(64, "#b50000", 1, DASHED_LINE, 4, 0),
                new Waveline(32, "#ff0000", 1, STRAIGHT_LINE, 3, 0),
                new Waveline(16, "#ff7700", 1, STRAIGHT_LINE, 2, 0),
                new Waveline(4, "#ffe600", 1, STRAIGHT_LINE, 1, 0)
        )));

        // JungleNeon
        saveNewFilter(filterRepository, wavelineRepository,
        "JungleNeon", new ArrayList<>(Arrays.asList(
                new Waveline(4, "#7EC61A", 0.45f, STRAIGHT_LINE, 20, 0),
                new Waveline(4, "#ADFF39", 1, STRAIGHT_LINE, 15, 0),
                new Waveline(4, "#C5FFC5", 1, STRAIGHT_LINE, 9, 0)
        )));

        // Minimalist
        saveNewFilter(filterRepository, wavelineRepository,
                "Minimalist", new ArrayList<>(Arrays.asList(
                        new Waveline(16, "#ffffff", 1, DASHED_LINE, 3, 0)
                )));

        // Oceaned
        saveNewFilter(filterRepository, wavelineRepository,
                "Oceaned", new ArrayList<>(Arrays.asList(
                        new Waveline(256, "#0033cc", 1, STRAIGHT_LINE, 20, 0),
                        new Waveline(128, "#0f7bff", 1, STRAIGHT_LINE, 15, 0),
                        new Waveline(64, "#0fc3ff", 1, STRAIGHT_LINE, 10, 0)
                )));

        // Starked
        saveNewFilter(filterRepository, wavelineRepository,
                "Starked", new ArrayList<>(Arrays.asList(
                        new Waveline(16, "#ffffff", 0.4f, STRAIGHT_LINE, 3, 0),
                        new Waveline(4, "#ffffff", 1, STRAIGHT_LINE, 1, 0)
                )));

        // Sunshine
        saveNewFilter(filterRepository, wavelineRepository,
                "Sunshine", new ArrayList<>(Arrays.asList(
                        new Waveline(32, "#ffa600", 0.7f, STRAIGHT_LINE, 5, 0),
                        new Waveline(16, "#ffd500", 0.8f, STRAIGHT_LINE, 5, 0),
                        new Waveline(4, "#fffb00", 1, STRAIGHT_LINE, 3, 0)
                )));

        // Synthwave
        saveNewFilter(filterRepository, wavelineRepository,
        "Synthwave", new ArrayList<>(Arrays.asList(
                new Waveline(4, "#D900FF", 1, STRAIGHT_LINE, 4, -10),
                new Waveline(4, "#00FFC4", 1, STRAIGHT_LINE, 4, 10),
                new Waveline(4, "#FFE600", 1, STRAIGHT_LINE, 4, 1)
        )));

        // ThreeDee
        saveNewFilter(filterRepository, wavelineRepository,
        "ThreeDee", new ArrayList<>(Arrays.asList(
                new Waveline(4, "#0088FF", 0.6f, STRAIGHT_LINE, 4, -10),
                new Waveline(4, "#FF0000", 0.6f, STRAIGHT_LINE, 4, 10)
        )));

        // Tumult
        saveNewFilter(filterRepository, wavelineRepository,
        "Tumult", new ArrayList<>(Arrays.asList(
                new Waveline(16, "#9000ff", 1, STRAIGHT_LINE, 1, 0),
                new Waveline(4, "#00fffb", 1, STRAIGHT_LINE, 1, 0)
        )));
    }

	private void saveNewFilter(FilterRepository filterRepository, WavelineRepository wavelineRepository,
							   String name, ArrayList<Waveline> wavelines) {
		Filter new_filter = new Filter(name);
		new_filter.setWavelines(wavelines);
		filterRepository.save(new_filter);
		for(Waveline waveline : wavelines)
			wavelineRepository.save(waveline);
	}
//
//	private void cleanGenreFile(File dirtyFile) throws IOException {
//
//		// Get all labels from dirty file
//		BufferedReader br = new BufferedReader(new FileReader(dirtyFile));
//		List<String> labels = new ArrayList<>();
//		String label;
//		while ((label = br.readLine()) != null) {
//			labels.add(label);
//		}
//
//		// Remove duplicates and order alphabetically
//		labels = labels.stream()
//					   .distinct()
//				       .sorted(Comparator.comparing(n -> n))
//				       .collect(Collectors.toList());
//
//		// Clean the file content
//		PrintWriter pw = new PrintWriter(dirtyFile.getAbsolutePath());
//		pw.close();
//
//		// Write into the file with clean and organized data
//		FileWriter writer = new FileWriter(dirtyFile.getAbsolutePath(), true);
//		for(String labelClean : labels) {
//			writer.write(labelClean.trim() + "\n");
//		}
//
//		writer.close();
//	}
//
//	@Bean
//	CommandLineRunner init_visuals(VisualRepository repository) {
//		return (args) -> {
//			repository.deleteAll();
//		};
//	}

}
