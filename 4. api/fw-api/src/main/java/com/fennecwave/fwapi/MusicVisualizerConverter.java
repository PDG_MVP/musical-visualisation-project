package com.fennecwave.fwapi;

import com.fennecwave.fwapi.model.Filter;
import com.fennecwave.fwapi.model.Waveline;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import javax.sound.sampled.*;
import java.awt.*;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Project : FennecWave - Music Visualization Project
 * Author  : Stephane Bottin
 * Date    : 6.10.2020
 *
 * This class is used to convert a .wav file into a png
 */

public class MusicVisualizerConverter {

    public static final String STRAIGHT_LINE = "STRAIGHT";
    public static final String DASHED_LINE = "DASHED";

    // If the sound is below that threshold it will not be taken into account
    // TODO : Add as an option maybe ?
    private static final int SILENCE_THRESHOLD = 5;

    public BufferedImage generateRepresentation(AudioInputStream audioInputStream, Filter filter, int width) {

        final int BASE_RADIUS = width / 5;
        final int IMG_CENTER = width / 2;
        final int VISUAL_WIDTH = width / 15;

        BufferedImage img = new BufferedImage(width, width,
                BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = img.createGraphics();

        // Background and configuration
        g2d.setComposite(AlphaComposite.Clear);
        g2d.fillRect(0, 0, width, width);
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Get subsets with no silence
        ArrayList<Float> noSilenceSubsets = null;
        try {
            noSilenceSubsets = getSubsetsWithoutStartAndEndSilence(getSubsets(audioInputStream, width, IMG_CENTER));
            //noSilenceSubsets = getSubsetsWithoutStartAndEndSilence(getSubsets(mp3File, width, IMG_CENTER));
        } catch (IOException | UnsupportedAudioFileException e) {
            e.printStackTrace();
        }

        // front thin lines
        g2d.setComposite(AlphaComposite.Src);
        drawPath(g2d, new ArrayList<>(filter.getWavelines()), noSilenceSubsets, width, VISUAL_WIDTH, IMG_CENTER, BASE_RADIUS);

        return img;
    }

    private float[] getSubsets(AudioInputStream in, int width, int center) throws IOException, UnsupportedAudioFileException {
        float[] samples;

        AudioFormat fmt = in.getFormat();

        if (fmt.getEncoding() != AudioFormat.Encoding.PCM_SIGNED) {
            throw new UnsupportedAudioFileException("unsigned");
        }

        boolean big = fmt.isBigEndian();
        int chans = fmt.getChannels();
        int bits = fmt.getSampleSizeInBits();
        int bytes = bits + 7 >> 3;

        int frameLength = (int) in.getFrameLength();
        int bufferLength = chans * bytes * 1024;

        // To avoid heap space errors
        while (frameLength > 50000000) {
            frameLength /= 2;
        }

        samples = new float[frameLength];
        byte[] buf = new byte[bufferLength];

        int i = 0;
        int bRead;
        System.out.println(samples.length);
        while ( ( bRead = in.read(buf) ) > -1) {

            for (int b = 0; b < bRead;) {
                double sum = 0;

                // (sums to mono if multiple channels)
                for (int c = 0; c < chans; c++) {
                    if (bytes == 1) {
                        sum += buf[b++] << 8;

                    } else {
                        int sample = 0;

                        // (quantizes to 16-bit)
                        if (big) {
                            sample |= ( buf[b++] & 0xFF ) << 8;
                            sample |= ( buf[b++] & 0xFF );
                            b += bytes - 2;
                        } else {
                            b += bytes - 2;
                            sample |= ( buf[b++] & 0xFF );
                            sample |= ( buf[b++] & 0xFF ) << 8;
                        }

                        final int sign = 1 << 15;
                        final int mask = -1 << 16;
                        if ( ( sample & sign ) == sign) {
                            sample |= mask;
                        }

                        sum += sample;
                    }
                }

                if(i < samples.length)
                    samples[i++] = (float) ( sum / chans );
            }
        }

        int subsetLength = samples.length / width;

        float[] result = new float[width];

        // find average(abs) of each box subset
        int s = 0;
        for (int j = 0; j < result.length; j++) {

            double sum = 0;
            for (int k = 0; k < subsetLength; k++) {
                sum += Math.abs(samples[s++]);
            }

            result[j] = (float) ( sum / subsetLength );
        }

        // find the peak so the waveform can be normalized
        // to the height of the image
        float normal = 0;
        for (float sample : result) {
            if (sample > normal)
                normal = sample;
        }

        // normalize and scale
        normal = 32768.0f / normal;
        for (int j = 0; j < result.length; j++) {
            result[j] *= normal;
            result[j] = ( result[j] / 32768.0f ) * center;
        }

        return result;
    }


    private ArrayList<Float> getSubsetsWithoutStartAndEndSilence(float[] subsets) {
        ArrayList<Float> result = new ArrayList<>();
        boolean started = false;

        int endIndex = -1;
        // detect index of last relevant data
        for(int i = 0; i < subsets.length; i++) {
            if(i >= endIndex && subsets[i] > SILENCE_THRESHOLD)
                endIndex = i;
        }

        // Add the relevant data without silent start
        for (int i = 1; i < subsets.length; i++) {
            if(!started) {
                if(subsets[i] <= SILENCE_THRESHOLD) {
                    // Silent start, we don't add it
                } else {
                    result.add(subsets[i]);
                    started = true;
                }
            } else {
                if(i > endIndex) {
                    // Silent end, we don't add it
                } else {
                    result.add(subsets[i]);
                }
            }

        }

        return result;
    }

    private void drawPath(Graphics2D graphics, ArrayList<Waveline> wavelines, ArrayList<Float> subsets,
                          int width, int visual_width, int center, int radius) {

        float max_subset_height = Collections.max(subsets);

        for(Waveline waveline : wavelines) {

            GeneralPath posLinePath = new GeneralPath();
            GeneralPath negLinePath = new GeneralPath();
            posLinePath.moveTo(width / 2,(width / 2) - radius);
            negLinePath.moveTo(width / 2,(width / 2) - radius);

            // To get a smooth curved line we will draw cubic curves for each group of 4 subsets
            for (int i = waveline.getNoise(); i < subsets.size(); i += (waveline.getNoise() - 1)) {

                // 4 : 0, 1, 2, 3
                //     3, 4, 5, 6
                //     ...


                // To draw a cubic curve we need 4 points :
                // x, y         : the starting and ending points
                // ctrl1, ctrl2 : the control points for the cubic curvature
                // First we get the index of the 4 points depending on the noise of the waveline
                int index_x     = i - waveline.getNoise();
                int index_ctrl1 = i - (int) Math.ceil((waveline.getNoise() * 2) / 3.0);
                int index_ctrl2 = i - (int) Math.ceil(waveline.getNoise() / 3.0);
                int index_y     = i - 1;
                // Then we get the samples form those points
                int sample_x     = (int) ((float) subsets.get(index_x));
                int sample_ctrl1 = (int) ((float) subsets.get(index_ctrl1));
                int sample_ctrl2 = (int) ((float) subsets.get(index_ctrl2));
                int sample_y     = (int) ((float) subsets.get(index_y));
                // And the angle of those points
                float angular_offset = -90;
                float alpha_x     = ((index_x     / (float) subsets.size()) * 360) + angular_offset;
                float alpha_ctrl1 = ((index_ctrl1 / (float) subsets.size()) * 360) + angular_offset;
                float alpha_ctrl2 = ((index_ctrl2 / (float) subsets.size()) * 360) + angular_offset;
                float alpha_y     = ((index_y     / (float) subsets.size()) * 360) + angular_offset;

                // Get the negative and positive height from each points
                int posHeight_x = radius - sample_x; // positive height
                int negHeight_x = radius + sample_x; // negative height
                // Middle point
                int posHeight_ctrl1 = radius - sample_ctrl1; // positive height
                int negHeight_ctrl1 = radius + sample_ctrl1; // negative height
                // Middle point
                int posHeight_ctrl2 = radius - sample_ctrl2; // positive height
                int negHeight_ctrl2 = radius + sample_ctrl2; // negative height
                // Ending point
                int posHeight_y = radius - sample_y; // positive height
                int negHeight_y = radius + sample_y; // negative height

                // Height normalization for each sample height
                int normalized_posHeight_x     = getNormalizedPixelPosition(posHeight_x, max_subset_height, visual_width);
                int normalized_negHeight_x     = getNormalizedPixelPosition(negHeight_x, max_subset_height, visual_width);
                int normalized_posHeight_ctrl1 = getNormalizedPixelPosition(posHeight_ctrl1, max_subset_height, visual_width);
                int normalized_negHeight_ctrl1 = getNormalizedPixelPosition(negHeight_ctrl1, max_subset_height, visual_width);
                int normalized_posHeight_ctrl2 = getNormalizedPixelPosition(posHeight_ctrl2, max_subset_height, visual_width);
                int normalized_negHeight_ctrl2 = getNormalizedPixelPosition(negHeight_ctrl2, max_subset_height, visual_width);
                int normalized_posHeight_y     = getNormalizedPixelPosition(posHeight_y, max_subset_height, visual_width);
                int normalized_negHeight_y     = getNormalizedPixelPosition(negHeight_y, max_subset_height, visual_width);

                // Bezier curved
                switch(waveline.getStrokeStyle()) {
                    case STRAIGHT_LINE:
                        graphics.setStroke(new BasicStroke(waveline.getStrokeWidth(),
                                BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)
                        );
                        break;
                    case DASHED_LINE:
                        graphics.setStroke(new BasicStroke(waveline.getStrokeWidth(),
                                BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 9f,
                                new float[] {9f}, 0f)
                        );
                        break;
                }

                // Color
                AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, waveline.getAlpha());
                graphics.setComposite(alcom);
                graphics.setColor(Color.decode(waveline.getColor()));

                // upper line
                drawCubicCurve(graphics, alpha_x, alpha_ctrl1, alpha_ctrl2, alpha_y, normalized_posHeight_x,
                        normalized_posHeight_ctrl1, normalized_posHeight_ctrl2, normalized_posHeight_y, radius,
                        center, waveline.getRandFactor());
                // lower line
                drawCubicCurve(graphics, alpha_x, alpha_ctrl1, alpha_ctrl2, alpha_y, normalized_negHeight_x,
                        normalized_negHeight_ctrl1, normalized_negHeight_ctrl2, normalized_negHeight_y, radius,
                        center, waveline.getRandFactor());
            }
        }
    }

    private void drawCubicCurve(Graphics2D graphics, float alpha_x, float alpha_ctrl1, float alpha_ctrl2,
                                float alpha_y, int x, int ctrl1, int ctrl2, int y, int radius, int center, int randFactor) {
        Random random = new Random();
        graphics.draw(new CubicCurve2D.Double(
                (Math.cos(Math.toRadians(alpha_x)) * (radius + x)) + center + (random.nextDouble() * randFactor),
                (Math.sin(Math.toRadians(alpha_x)) * (radius + x)) + center + (random.nextDouble() * randFactor),
                (Math.cos(Math.toRadians(alpha_ctrl1)) * (radius + ctrl1)) + center + (random.nextDouble() * randFactor),
                (Math.sin(Math.toRadians(alpha_ctrl1)) * (radius + ctrl1)) + center + (random.nextDouble() * randFactor),
                (Math.cos(Math.toRadians(alpha_ctrl2)) * (radius + ctrl2)) + center + (random.nextDouble() * randFactor),
                (Math.sin(Math.toRadians(alpha_ctrl2)) * (radius + ctrl2)) + center + (random.nextDouble() * randFactor),
                (Math.cos(Math.toRadians(alpha_y)) * (radius + y)) + center + (random.nextDouble() * randFactor),
                (Math.sin(Math.toRadians(alpha_y)) * (radius + y)) + center + (random.nextDouble() * randFactor)
        ));
    }

    private int getNormalizedPixelPosition(int pos, float max, int width) {
        return (int) ((pos * width) / max);
    }

    private void displayMetadata(File file) {
        System.out.println("Analysing : " + file.getName());

        //detecting the file type
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        FileInputStream inputstream = null;
        try {
            inputstream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ParseContext pcontext = new ParseContext();

        //Mp3 parser
        //WavParser Mp3Parser = new  Mp3Parser();
        //LyricsHandler lyrics = null;
        /*try {
        //    Mp3Parser.parse(inputstream, handler, metadata, pcontext);
        //    lyrics = new LyricsHandler(inputstream,handler);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        }*/

        /*while(lyrics.hasLyrics()) {
            System.out.println(lyrics.toString());
        }*/

        System.out.println("Contents of the document:" + handler.toString());
        System.out.println("Metadata of the document:");
        String[] metadataNames = metadata.names();

        for(String name : metadataNames) {
            System.out.println(name + ": " + metadata.get(name));
        }
    }

    /**
     * Converts a mp3 file into a wav file
     * @param mp3Data : mp3 file
     * @throws UnsupportedAudioFileException
     * @throws IOException
     */
    public static File mp3ToWav(File mp3Data) throws UnsupportedAudioFileException, IOException {
        // open stream
        AudioInputStream mp3Stream = AudioSystem.getAudioInputStream(mp3Data);
        AudioFormat sourceFormat = mp3Stream.getFormat();
        // create audio format object for the desired stream/audio format
        // this is *not* the same as the file format (wav)
        AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                sourceFormat.getSampleRate(), 16,
                sourceFormat.getChannels(),
                sourceFormat.getChannels() * 2,
                sourceFormat.getSampleRate(),
                false);
        // create stream that delivers the desired format
        AudioInputStream converted = AudioSystem.getAudioInputStream(convertFormat, mp3Stream);
        // write stream into a file with file format wav
        File wavFile =  new File(mp3Data.getParent() + "/" +
                mp3Data.getName().substring(0, mp3Data.getName().lastIndexOf('.')) + ".wav");
        AudioSystem.write(converted, AudioFileFormat.Type.WAVE, wavFile);

        // delete the mp3 file
        mp3Data.delete();

        return wavFile;
    }

    public static float getAudioLengthInSeconds(File audioFile) throws IOException, UnsupportedAudioFileException {
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(audioFile);
        AudioFormat format = audioInputStream.getFormat();
        long audioFileLength = audioFile.length();
        int frameSize = format.getFrameSize();
        float frameRate = format.getFrameRate();

        return audioFileLength / (frameSize * frameRate);
    }
}
