package com.fennecwave.fwapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
@Entity
public class Genre {

    @Id
    private String label;
}
