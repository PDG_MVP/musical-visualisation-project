package com.fennecwave.fwapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
public class UserVisualLike {

    @JsonIgnore
    @EmbeddedId
    UserVisualLikeKey id;

    Date time;

    // Username only to avoid stack overflow
    @PostLoad
    private void postLoad() {
        username = user.getUsername();
    }
    @Transient // To avoid infinite recursion when deserializing
    private String username;

    @JsonIgnore
    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    User user;

    @JsonIgnore
    @ManyToOne
    @MapsId("visualId")
    @JoinColumn(name = "visual_id")
    Visual visual;


}

