package com.fennecwave.fwapi.repository;

import com.fennecwave.fwapi.model.Genre;
import com.fennecwave.fwapi.model.Visual;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre, String> {
    @Query(value = "SELECT DISTINCT v FROM Visual v INNER JOIN v.genres vg WHERE vg.label = ?1")
    List<Visual> findVisualsByGenre(String label);
}
