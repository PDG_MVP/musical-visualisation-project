package com.fennecwave.fwapi;


import com.fennecwave.fwapi.service.storage.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)	//TODO check if still necessary
public class FwApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FwApiApplication.class, args);
	}

}
