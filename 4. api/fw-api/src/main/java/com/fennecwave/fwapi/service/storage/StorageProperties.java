package com.fennecwave.fwapi.service.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties("storage")
public class StorageProperties {

	/**
	 * Folder location for storing files
	 */
	private String location = "upload-dir";

	private String tempLocation = "upload-temp";

	public String getLocation() {
		return location;
	}

	public String getTempLocation() {
		return tempLocation;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}