package com.fennecwave.fwapi.repository;

import com.fennecwave.fwapi.model.Visual;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VisualRepository extends JpaRepository<Visual, Long> {
    Optional<List<Visual>> findByTitle(String title);
    Optional<List<Visual>> findBySubtitle(String subtitles);
    Optional<List<Visual>> findByAuthor(String author);

    @Query("SELECT v FROM Visual v INNER JOIN UserVisualLike l " +
            "ON v.id_visual = l.visual.id_visual " +
            "WHERE v.author = :author GROUP BY v.id_visual")
    List<Visual> getAllFavouritedVisuals(@Param("author") String author);

    @Query("SELECT v FROM Visual v WHERE v.author <> ''")
    List<Visual> getAllAuthoredVisuals();

    @Query("SELECT v FROM Visual v ORDER BY v.likes.size DESC")
    List<Visual> findByTrends();
}
