package com.fennecwave.fwapi.controller;

import com.fennecwave.fwapi.model.Genre;
import com.fennecwave.fwapi.model.Visual;
import com.fennecwave.fwapi.repository.GenreRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/genres"})
public class GenreController {

    private GenreRepository repository;

    GenreController(GenreRepository genreRepository) {
        this.repository = genreRepository;
    }

    // Find all genres
    @GetMapping
    public List findAll(){
        return repository.findAll();
    }

    // Create a new genre
    @PostMapping
    public Genre create(@RequestBody Genre genre) {
        return repository.save(genre);
    }

    // Find all visuals in genre
    @GetMapping(path = {"/{label}"})
    public List<Visual> findVisuals(@PathVariable String label){
        return repository.findVisualsByGenre(label);
    }

}
