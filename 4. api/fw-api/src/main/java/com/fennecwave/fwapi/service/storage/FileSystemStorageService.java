package com.fennecwave.fwapi.service.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
public class FileSystemStorageService implements StorageService {

	private final Path rootLocation;
	private final Path tempLocation;

	private final int purgeInterval = 60000*60*24;

	@Autowired
	public FileSystemStorageService(StorageProperties properties) throws InterruptedException {
		this.rootLocation = Paths.get(properties.getLocation());
		this.tempLocation = Paths.get(properties.getTempLocation());

		Thread tempCleaner = new Thread() {
			public void run() {
				while(true) {
					try {
						purgeDirectory(tempLocation.toFile());
						System.out.println("LOG : temp directory purged");
						Thread.sleep(purgeInterval);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		tempCleaner.start();

	}

	private void purgeDirectory(File tempDirectory) {
		for (File file: tempDirectory.listFiles()) {
			if (file.isDirectory())
				purgeDirectory(file);
			file.delete();
		}
	}

	@Override
	public void storeTemp(File file) {
		try {
			/*if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file.");
			}*/
			Path destinationFile = this.tempLocation.resolve(
					Paths.get(file.getName()))
					.normalize().toAbsolutePath();
			System.out.println("Storing in : " + this.tempLocation.toAbsolutePath());
			if (!destinationFile.getParent().equals(this.tempLocation.toAbsolutePath())) {
				// This is a security check
				throw new StorageException(
						"Cannot store file outside current directory. " + destinationFile.getParent().toString());
			}
			Files.copy(file.toPath(), destinationFile, REPLACE_EXISTING);
		}
		catch (IOException e) {
			throw new StorageException("Failed to store file.", e);
		}
	}

	@Override
	public void save(String filename) {
		Path destinationFile = this.rootLocation.resolve(
				Paths.get(filename))
				.normalize().toAbsolutePath();

		// Security check
		if (!destinationFile.getParent().equals(this.rootLocation.toAbsolutePath())) {
			throw new StorageException("Cannot store file outside current directory. "
					+ destinationFile.getParent().toString());
		}
		try {
			Files.copy(tempLocation.resolve(filename), destinationFile, REPLACE_EXISTING);
		} catch (IOException e) {
			throw new StorageException("Failed to store file.", e);
		}
	}

	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootLocation, 1)
				.filter(path -> !path.equals(this.rootLocation))
				.map(this.rootLocation::relativize);
		}
		catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}

	}

	@Override
	public Path load(String filename) {
		return rootLocation.resolve(filename);
	}

	@Override
	public Path loadTemp(String filename) {
		return tempLocation.resolve(filename);
	}

	@Override
	public Resource loadAsResource(String filename, boolean temp) {
		try {
			Path file;
			if(temp) {
				file = loadTemp(filename);
			} else {
				file = load(filename);
			}
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			}
			else {
				throw new StorageFileNotFoundException(
						"Could not read file: " + filename);

			}
		}
		catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	@Override
	@PostConstruct
	public void init() {
		try {
			Files.createDirectories(rootLocation);
			Files.createDirectories(Path.of("temp"));
			Files.createDirectories(Path.of("generated"));
			Files.createDirectories(Path.of("upload-temp"));
		}
		catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}
}
