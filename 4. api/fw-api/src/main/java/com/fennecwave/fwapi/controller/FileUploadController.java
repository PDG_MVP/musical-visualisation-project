package com.fennecwave.fwapi.controller;

import com.fennecwave.fwapi.MusicVisualizerConverter;
import com.fennecwave.fwapi.model.Filter;
import com.fennecwave.fwapi.model.Genre;
import com.fennecwave.fwapi.model.User;
import com.fennecwave.fwapi.model.Visual;
import com.fennecwave.fwapi.repository.FilterRepository;
import com.fennecwave.fwapi.repository.UserRepository;
import com.fennecwave.fwapi.repository.VisualRepository;
import com.fennecwave.fwapi.service.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


@RestController
@RequestMapping({"/upload"})
public class FileUploadController {
    @Value("${DOCKER-API-URL}")
    private String baseApiDockerUrl;

    @Value("${DOCKER-API-URL-EXT}")
    private String baseApiDockerExternalUrl;

    private final String fileServeURL;
    private final String fileServeTempURL;

    private final UserRepository userRepository;
    private final VisualRepository visualRepository;
    private final FilterRepository filterRepository;
    private final MusicVisualizerConverter converter;
    private final StorageService storageService;

    private final Path tempPath;
    private final Path generatedPath;

    @Autowired
    public FileUploadController(UserRepository userRepository, FilterRepository filterRepository, VisualRepository visualRepository, StorageService storageService) {
        this.userRepository = userRepository;
        this.visualRepository = visualRepository;
        this.filterRepository = filterRepository;
        this.converter = new MusicVisualizerConverter();
        this.storageService = storageService;
        this.tempPath = Path.of("temp");
        this.generatedPath = Path.of("generated");
        this.fileServeURL ="/upload/files/?filename=";
        this.fileServeTempURL ="/upload/tempFiles/?filename=";

        try {
            Files.createDirectories(tempPath);
            Files.createDirectories(generatedPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/files")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@RequestParam("filename") String filename, HttpServletRequest request) throws IOException {
        Resource resource = storageService.loadAsResource(filename, false);
        return getResourceResponseEntity(request, resource);

    }


    @GetMapping("/tempFiles")
    @ResponseBody
    public ResponseEntity<Resource> getTempFile(@RequestParam("filename") String filename, HttpServletRequest request) throws IOException {
        Resource resource = storageService.loadAsResource(filename, true);
        return getResourceResponseEntity(request, resource);

    }

    // TODO : Replace to response entity ?
    @PostMapping
    public Visual handleFileUpload(
            HttpServletRequest request,
            @RequestParam(value = "file", required = false) MultipartFile multipartAudioFile,
            @RequestParam(value = "filename", required = false) String previousFilename,
            @RequestParam("filterId") Long filterId,
            @RequestParam("title") String title,
            @RequestParam("subtitle") String subtitle,
            @RequestParam(value = "author", required = false) String author
            /*TODO : @RequestParam("genres") List<String> genres*/) throws UnsupportedAudioFileException {

        File audioFile;

        if(multipartAudioFile != null) {
            // Save file in /temp to allow regeneration of the visual without uploading more than once
            // TODO : delete after being saved in the api as a visual or after a timeout
            saveFile(multipartAudioFile, tempPath);
            audioFile = new File(tempPath.toString() + "/" + multipartAudioFile.getOriginalFilename());
        } else if (previousFilename != null && !previousFilename.isEmpty()){
            audioFile = new File(tempPath.toString() + "/" + previousFilename);
        } else {
            System.out.println("Error : The audio file and the previous filename are missing.");
            return null;
        }

        // If mp3 we can convert it to wav
        if(getFileExtension(audioFile).equals(".mp3")) {
            try {
                audioFile = MusicVisualizerConverter.mp3ToWav(audioFile);
            } catch (IOException e) {
                System.out.println("Error : The mp3 file could not be converted to a wav file");
                return null;
            }
        }

        // If it's a wav file we can generate the visual
        if(getFileExtension(audioFile).equals(".wav"))
        {
            try {
                // Get the generated image with the proper filter
                Filter filter = filterRepository.findById(filterId).get();
                BufferedImage img = createVisualFromAudioFile(audioFile, filter);

                // Save it (in folder and database) and return the visual
                return visualFromImage(title, subtitle, img, audioFile, filter, author);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            throw new UnsupportedAudioFileException();
        }

        return null;
    }


    @PostMapping("/save")
    public void save(@RequestParam("idVisual") Long idVisual,
                     @RequestParam("title") String title,
                     @RequestParam("subtitle") String subtitle,
                     @RequestParam("length") String length,
                     @RequestParam("genres") String genres,
                     @RequestParam("pathServed") String pathServed,
                     @RequestParam("author") String author) {
        Date now = new Date();

        // Create the visual object to then save in database if the author is valid
        Visual visual = Visual.builder()
                .id_visual(idVisual)
                .title(title)
                .subtitle(subtitle)
                .length(length)
                .pathServed(pathServed)
                .creationDate(now)
                .modificationDate(now)
                .genres(convertStrToGenre(genres))
                .build();

        // Save to database only if there is a valid author
        if(author != null && !author.equals("")) {
            Optional<User> userOption = userRepository.findByUsername(author);
            if (userOption.isPresent()) {
                // User is valid : saving the visual into the database
                visual.setAuthor(userOption.get().getUsername());
                String filename = visual.getPathServed().substring(visual.getPathServed().lastIndexOf('=') + 1);
                visual.setPathServed(baseApiDockerExternalUrl + fileServeURL + filename);
                visualRepository.save(visual);
                storageService.save(filename);
            }
        }
    }

    /**
     * Creates a PNG buffered image from an audio file
     * @param audioFile : audio file to convert
     * @param filter : filter used for the generation
     * @return Image of the generated visual
     * @throws IOException
     * @throws UnsupportedAudioFileException
     */
    private BufferedImage createVisualFromAudioFile(File audioFile, Filter filter)
            throws IOException, UnsupportedAudioFileException {

        //File uploadedFile = new File(tempPath.toString() + "/" + filename);
        AudioInputStream inputStream = AudioSystem.getAudioInputStream(audioFile);

        return converter.generateRepresentation(inputStream, filter, 4000);
    }

    /**
     * Save a multipart file in a directory
     * @param file : file to save
     * @param dir : directory to save the file into
     * @return path of the newly created file
     */
    private String saveFile(MultipartFile file, Path dir) {
        Path filepath = Paths.get(dir.toString(), file.getOriginalFilename());

        try (OutputStream os = Files.newOutputStream(filepath)) {
            os.write(file.getBytes());
        }catch (IOException e){
            e.printStackTrace();
        }

        return filepath.toString();
    }


    private Visual visualFromImage(String title, String subtitle, BufferedImage img,
                                   File file, Filter filter, String author) throws Exception {

        // Save it in the API
        Date now = new Date();
        String filename = file.getName();
        String visualName = filename.substring(0, filename.indexOf('.')) + "_" + filter.getName() + "_" + now.getTime() + ".png";
        File generatedFile = new File(generatedPath.toString()  + "/" + visualName);
        ImageIO.write(img, "PNG", generatedFile);
        storageService.storeTemp(generatedFile);

        //Delete generated file from /generated
        generatedFile.delete();

        // Add placeholders in case text fields are empty
        if(title.isEmpty())
            title = filename.substring(0, filename.lastIndexOf('.'));
        if(subtitle.isEmpty())
            subtitle = "No Subtitle";

        // Create the visual
        Visual visual = new Visual();
        visual.setTitle(title);
        visual.setSubtitle(subtitle);
        visual.setAudioSrcFilename(filename);
        visual.setVisualimg(visualName);
        visual.setCreationDate(now);
        visual.setModificationDate(now);
        visual.setLength(getFormattedLength((long) MusicVisualizerConverter.getAudioLengthInSeconds(file)));
        visual.setGenres(new ArrayList<>());
        visual.setPathServed(baseApiDockerExternalUrl + fileServeTempURL + visualName);

        return visual;
    }

    private List<Genre> convertStrToGenre(String genres){
        if(genres == null || genres.equals("")){
            return new ArrayList<>();
        }

        List<String> genresStr =  Arrays.asList(genres.split(","));

        List<Genre> result = new ArrayList<>();
        for(String genre : genresStr)
            result.add(new Genre(genre));

        return result;
    }

    /**
     * Get the file extension of a file
     * @param file : file
     * @return file extension
     */
    private static String getFileExtension(File file) {
        if(file == null || file.getName().isEmpty())
            return "";

        return file.getName().substring(file.getName().lastIndexOf("."));
    }

    /**
     * Get formatted length from seconds to "MM:SS"
     * @param seconds : length in seconds
     * @return formatted string "MM:SS"
     */
    private static String getFormattedLength(long seconds) {
        long min = seconds / 60;
        long sec = seconds % 60;
        return (min < 10 ? "0" : "") + min + ":"
                + (sec < 10 ? "0" : "") + sec;
    }

    private ResponseEntity<Resource> getResourceResponseEntity(HttpServletRequest request, Resource resource) {
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType)).header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
    }


}

