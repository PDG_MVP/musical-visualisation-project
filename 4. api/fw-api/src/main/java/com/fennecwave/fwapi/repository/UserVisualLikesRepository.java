package com.fennecwave.fwapi.repository;

import com.fennecwave.fwapi.model.UserVisualLike;
import com.fennecwave.fwapi.model.UserVisualLikeKey;
import com.fennecwave.fwapi.model.Visual;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserVisualLikesRepository extends JpaRepository<UserVisualLike, UserVisualLikeKey> {

    @Query("SELECT v FROM UserVisualLike v WHERE v.user.username = :username AND v.visual.id_visual = :idVisual")
    Optional<UserVisualLike> getByUserAndVisual(@Param("username") String username,
                                                @Param("idVisual") Long idVisual);

}
