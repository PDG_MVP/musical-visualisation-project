package com.fennecwave.fwapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
@Entity
public class Filter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "filter", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private List<Waveline> wavelines;

    public Filter(String name) {
        this.name = name;
        wavelines = new ArrayList<>();
    }

    public void setWavelines(ArrayList<Waveline> wavelines) {
        this.wavelines = wavelines;
        for(Waveline waveline : this.wavelines) {
            waveline.setFilter(this);
        }
    }
}
