package com.fennecwave.fwapi.controller;

import com.fennecwave.fwapi.model.User;
import com.fennecwave.fwapi.model.UserVisualLike;
import com.fennecwave.fwapi.model.UserVisualLikeKey;
import com.fennecwave.fwapi.model.Visual;
import com.fennecwave.fwapi.repository.UserRepository;
import com.fennecwave.fwapi.repository.UserVisualLikesRepository;
import com.fennecwave.fwapi.repository.VisualRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.html.Option;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping({"/users"})
public class UserController {

    private UserRepository repository;
    private VisualRepository visualRepository;
    private UserVisualLikesRepository likesRepository;


    @Autowired
    UserController(UserRepository userRepository, VisualRepository visualRepository,
                   UserVisualLikesRepository likesRepository) {
        this.repository = userRepository;
        this.visualRepository = visualRepository;
        this.likesRepository = likesRepository;
    }

    // Register
    @PostMapping({"/register"})
    @ResponseBody
    public Boolean create(@RequestBody User user) {
        System.out.println("Created user :"+user);
        if(repository.findByUsername(user.getUsername()).isPresent())
        {
            System.out.println("Already exists : " + repository.findByUsername(user.getUsername()));
            return false;
        }
        repository.save(user);
        return true;
    }

    // Login (Returns the password for the given username)
    @PostMapping({"/login"})
    @ResponseBody
    public String login(@RequestBody User user) {
        User stored_user = repository.findByUsername(user.getUsername()).orElseThrow(InvalidCredentialsException::new);
        return stored_user.getPassword();
    }

    // Find all users
    @GetMapping
    public List findAll(){
        System.out.println("Getting all users");
        return repository.findAll();
    }

    // Get user by id
    @GetMapping(path = {"/{id}"})
    public ResponseEntity<User> findById(@PathVariable long id){
        System.out.println("Getting user " + id);//DEBUG
        return repository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    // Likes
    // TODO : WARNING SECURITY ISSUE, ANYONE CAN MAKE ANYONE ELSE LIKE A VISUAL /!\
    // TODO : Remove username from parameters and access it with session ?
    @PostMapping(path = {"/like"})
    public void likeVisual(@RequestParam Long visualId, @RequestParam String username) {
        System.out.println("Receiving a like request");

        Optional<UserVisualLike> like = likesRepository.getByUserAndVisual(username, visualId);

        // Like is already made = unlike
        if(like.isPresent()) {
            likesRepository.delete(like.get());
        } else {
            // New like, check that the user and the visual are valid
            Optional<User> user = repository.findByUsername(username);
            Optional<Visual> visual = visualRepository.findById(visualId);

            if(user.isPresent() && visual.isPresent()) {
                // Parameters are valid : we add the like
                likesRepository.save(new UserVisualLike(
                        new UserVisualLikeKey(), new Date(),
                        user.get().getUsername(),
                        user.get(), visual.get())
                );
            }
        }
    }

    // Update a user
    @PutMapping(value="/{id}")
    public ResponseEntity<User> update(@PathVariable("id") long id,
                                       @RequestBody User user){
        return repository.findById(id)
                .map(record -> {
                    record.setUsername(user.getUsername());
                    record.setPassword(user.getPassword());
                    record.setLikes(user.getLikes());
                    User updated = repository.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    // Delete a user
    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        return repository.findById(id)
                .map(record -> {
                    repository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }

    /*BEANS*/



    /*EXCEPTIONS*/
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid user credentials")
    public static class InvalidCredentialsException extends RuntimeException {}
}
