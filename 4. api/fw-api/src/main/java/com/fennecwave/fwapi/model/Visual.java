package com.fennecwave.fwapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
@Entity
@Builder
public class Visual {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_visual;

    private String title;
    private String subtitle;
    private String visualimg;
    private String audioSrcFilename;

    private Date creationDate;
    private Date modificationDate;
    private String length;
    private String author;

    private String pathServed;

    @ManyToMany
    @JoinTable(
            name = "Visual_Genre",
            joinColumns = @JoinColumn(name = "visual_id", referencedColumnName = "id_visual"),
            inverseJoinColumns = @JoinColumn(name = "genre_label", referencedColumnName = "label")
    )
    private List<Genre> genres;

    // Likes
    @OneToMany(mappedBy = "visual")
    @OnDelete(action = OnDeleteAction.CASCADE)
    List<UserVisualLike> likes;
}
