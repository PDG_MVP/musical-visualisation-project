package com.fennecwave.fwapi.service.storage;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Path;
import java.util.stream.Stream;


public interface StorageService {

	void init();

	void storeTemp(File file);

	void save(String filename);

	Stream<Path> loadAll();

	Path load(String filename);

	Path loadTemp(String filename);

	Resource loadAsResource(String filename, boolean temp);

	void deleteAll();

}
