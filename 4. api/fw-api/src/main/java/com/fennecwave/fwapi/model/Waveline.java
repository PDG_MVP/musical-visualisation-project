package com.fennecwave.fwapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
@Entity
public class Waveline {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int noise;
    private String color;
    private float alpha;
    private String strokeStyle;
    private int strokeWidth;
    private int randFactor;

    @ManyToOne
    @JsonIgnore
    private Filter filter;

    public Waveline(int noise, String color, float alpha, String strokeStyle,
                    int strokeWidth, int randFactor) {
        this.noise = noise;
        this.color = color;
        this.alpha = alpha;
        this.strokeStyle = strokeStyle;
        this.strokeWidth = strokeWidth;
        this.randFactor = randFactor;
    }
}
