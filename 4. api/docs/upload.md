# Upload

Used to upload music information and get the generated visual.

**URL** : `/fwapi/visuals/`

**Method** : `POST`

**Auth required** : YES (cf https://howtodoinjava.com/spring-boot2/security-rest-basic-auth-example/)

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
	{
		"title" : "Hotel California",
		"subtitle" : "Eagle",
		"visualImg" : "hotel_california_eagle_202021021_162237.png",
		"creationDate" : "2020-10-21:16h22m37s",
		"modificationDate" : "2020-10-21:16h22m37s",
		"lengthSeconds" : 390
		"author" : "aUser1234"
		"likes" : []
	}
}
```