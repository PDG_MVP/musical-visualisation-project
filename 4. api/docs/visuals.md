# Visuals

Used to generate a list of all visuals uploaded by any users

**URL** : `/fwapi/visuals/`

**Method** : `GET`

**Auth required** : NO

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
	{
		"title" : "Hotel California",
		"subtitle" : "Eagle",
		"visualImg" : "hotel_california_eagle_202021021_162237.png",
		"creationDate" : "2020-10-21:16h22m37s",
		"modificationDate" : "2020-10-21:16h22m37s",
		"lengthSeconds" : 390,
		"author" : "aUser1234",
		"likes" : [
			{
				"username" : "aUser1234"
			},
			{
				"username" : "aUser7890"
			}
		]
	},
	{
		"title" : "Hotel California",
		"subtitle" : "Eagle",
		"visualImg" : "hotel_california_eagle_202021021_162237.png",
		"creationDate" : "2020-10-21:16h22m37s",
		"modificationDate" : "2020-10-21:16h22m37s",
		"lengthSeconds" : 390,
		"author" : "aUser1234",
		"likes" : []
	}, etc...
}
```