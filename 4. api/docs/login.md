# Login

Used to collect a session token for a registered User.

**URL** : `/fwapi/login/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
    "username": "[valid username]",
    "password": "[encrypted password]"
}
```

**Data example**

```json
{
    "username": "aUser1234",
    "password": "63EE547CC162DF2C5FB1BEE02D294F7D5"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
	"status": {
	    "type": "success",
	    "message": "Success",
	    "code": 200,
	    "error": false
	},
	"data": [
		{
		    "status": "Authenticated",
		    "user": {
		        "username": "aUser1234",
		        "id": 88888888
		    },
		    "return_to_url": null,
		    "expires_at": "2020/01/07 05:56:21 +0000",
		    "session_token": "9x8869x31134x7906x6x54474x21x18xxx90857x"
		}
	]
}
```

## Error Response

**Condition** : If 'username' and 'password' combination or the JSON is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "status": {
        "code": 400,
        "error": true,
        "message": "Input JSON is not valid",
        "type": "bad request"
    }
}
```

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
    "status": {
      "code": 401,
      "error": true,
      "message": "Authentication Failed: Invalid user credentials",
      "type": "Unauthorized"
}
```

