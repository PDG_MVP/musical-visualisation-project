# Users

Used to generate a list of all users

**URL** : `/fwapi/users/`

**Method** : `GET`

**Auth required** : NO

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
	{
		"username" : "aUser1234"
	},
	{
		"username" : "aUser7890"
	}
}
```