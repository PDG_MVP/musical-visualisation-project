![FennecWave][logo]

# Turn your songs into art.

FennecWave is a website to turn your favourite songs into a unique visual representation. [Try now!](https://fennecwave.com)

## Table of contents

- [Introduction](#introduction)
- [Spectrum Analysis and Algorithm](#spectrum-analysis-and-algorithm)
- [Developement and Architecture](#developement-and-architecture)
- [Installation](#installation)
- [Testing](#testing)
- [Thanks](#thanks)
- [Contributions](#contributions)


## Introduction

This webapp project aims to provide visual representations of audio files for users to share. It was made by students at HEIG-VD for the PDG group project. All representation are copyright-free and can be used anywhere, at any time. If you want to experiment with our algorithm and try to make your own style, we welcome any contribution to make this the best free music visualizer of the year!

## Spectrum Analysis and Algorithm

### The math behind

In order to convert an horizontal spectrum visual into a circle, we just need to apply some basic trigonometry :

![schema-math][math]

### The Algorithm

For the algorithm, we simply analyse all the samples from a given .wav audio file and draw the lines on the circle using the previous technique with Java's ```Cubic Curves```. A ```Cubic Curve``` needs 4 points of reference so the circle is drawn by groups of four dots. A final visual is composed of generaly more than one waveline. So for each waveline we apply the same drawing technique. A waveline can have different attribute such as colour or the amount of noise we want to draw (smooth / harsh curves), here's an example of different filters on the same audio file : 

![schema-filters][filters]
 
## Developement and Architecture

For this project, we decided to make a CRUD REST API in Java with Spring Boot, Hibernate, JPA and MySQL.

We also use the API provided by [Songwhip](https://songwhip.com/) to search for streaming platforms.

### API Methods

The fwapi (Fennecwave API) gives access to different methods that enable our website or anyone to use this service : 

* [Users](4.%20api/docs/users.md) : `GET /fwapi/users/`
* [Visuals](4.%20api/docs/visuals.md) : `POST /fwapi/visuals/`
* [Upload](4.%20api/docs/upload.md) : `POST /fwapi/upload/`

[^1]: Basic HTTP login, secure ? Meh. Easy to implement for proof of concept ? Yeah...

## Installation
If you wish to host this service locally, you can follow this procedure.

### Setup
- Run ``git clone https://gitlab.com/PDG_MVP/musical-visualisation-project``

### Run
- Run ``docker-compose up``
- Open your browser to ``localhost:8081`` to use the web service or send your queries to ``localhost:8080`` to use the API
- Start visualizing!

If you wish to populate the application with sample data, you can run [e2e tests](#end-to-end-testing).

## Testing

### Unit Testing
From project base directory run :
```
cd 4.\ api/fw-api
mvn clean test
```

### Integration Testing

### End-to-end Testing
Our end-to-end tests use CodeceptJS with Puppeteer and FileSystem helpers.

The web service must be running. Follow the [installation](#installation) instructions to run the service locally.

From project base directory run :
```
cd 5.\ web/fw-web/codecept
npm import
npx codeceptjs run --steps
```

## Thanks

This project could not have been made without the help and motivation of :

* Olivier Liechti (Teacher in charge)
* Ludovic Delafontaine (Teaching assistant)

[logo]: 0.%20design/assets/fennec_logo_2.3_light.png "Logo Title Text 2"
[math]: 0.%20design/assets/algo-math.png "Logo Title Text 2"
[filters]: 0.%20design/assets/test-filters.png "Logo Title Text 2"

