
/*
 *  ==== ALL GENRES ====
 */

// Create a request variable and assign a new XMLHttpRequest object to it.
var requestUsers = new XMLHttpRequest()
// Open a new connection, using the GET request on the URL endpoint
requestUsers.open('GET', 'http://localhost:8080/genres', true)
requestUsers.onload = function () {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response)
  var genreList = document.getElementById('genreList');

  if (requestUsers.status >= 200 && requestUsers.status < 400) {

    data.forEach(genre => {
      var genreItem = document.createElement("div");
      genreItem.classList.add('genre-item');
      genreItem.innerHTML = genre.label;
      genreList.appendChild(genreItem);
    });

  } else {
    console.log('error')
  }
}
// Send request
requestUsers.send()