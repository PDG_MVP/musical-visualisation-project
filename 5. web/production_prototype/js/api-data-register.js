
/*
 *  ==== TOTAL USERS ====
 */

// Create a request variable and assign a new XMLHttpRequest object to it.
var requestUsers = new XMLHttpRequest()
// Open a new connection, using the GET request on the URL endpoint
requestUsers.open('GET', 'http://localhost:8080/users', true)
requestUsers.onload = function () {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response)

  if (requestUsers.status >= 200 && requestUsers.status < 400) {
    var totalUser = Object.keys(data).length;
    document.getElementById('subtitle-login').innerHTML = "Already " + totalUser + " user" + (totalUser > 1 ? "s are" : " is") + " sharing visuals !";
  } else {
    console.log('error')
  }
}
// Send request
requestUsers.send()

/*
 *  ==== REGISTER NEW USER ====
 */

function createNewUser() {
    var requestNewUser = new XMLHttpRequest();
    var url = 'http://localhost:8080/users';
    var userData = {
        'username' : '',
        'encodedPassword' : ''
    };

    userData.username = document.getElementById('form-input-username').value;
    userData.encodedPassword = document.getElementById('form-input-password').value;

    requestNewUser.open('POST', url, true);
    //Send the proper header information along with the request
    requestNewUser.setRequestHeader('Content-type', 'application/json');
    requestNewUser.onload = function () {
      if(requestNewUser.status == 200) { // OK -> redirect to login
        // Simulate an HTTP redirect (no going back allowed):
        window.location.replace("login.html");
      } else {

      }
    };

    requestNewUser.send(JSON.stringify(userData));
}