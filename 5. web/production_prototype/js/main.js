
/**
 * Open / Close top navigation hamburger
 */
document.addEventListener('DOMContentLoaded', () => {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
        // Add a click event on each of them
        $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {

                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }
});

/**
 * Toggle login form
 */
function toggleLogin() {
    var x = document.getElementById("login-form");
    if (x.style.display == "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

/**
 * Close login form
 */
function closeLogin() {
    var x = document.getElementById("login-form");
    x.style.display = "none";
}

function openSearchInput() {
    var searchInput = document.getElementById("search");
    var searchButton = document.getElementById("search-button");
    searchInput.style.display = "block";
    searchInput.focus();
    searchButton.style.display = "none";
}

function closeSearchInput() {
    var searchInput = document.getElementById("search");
    var searchButton = document.getElementById("search-button");
    searchInput.value = '';
    searchInput.style.display = "none";
    searchButton.style.display = "flex";
}