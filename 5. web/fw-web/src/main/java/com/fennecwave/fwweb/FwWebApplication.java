package com.fennecwave.fwweb;

import com.fennecwave.fwweb.models.Filter;
import com.fennecwave.fwweb.models.Genre;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class FwWebApplication {
	@Value("${DOCKER-API-URL}")
	private String baseApiDockerUrl;

	public static void main(String[] args) {
		SpringApplication.run(FwWebApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public CommandLineRunner loadFilters(RestTemplate restTemplate) {
		System.out.println("Loading filters...");
		return args -> {
			try {
				System.out.println("Fetching filters from " + baseApiDockerUrl + "/filters");
				Filter.APP_FILTERS = restTemplate.getForObject(
						baseApiDockerUrl + "/filters",
						Filter[].class);
				if(Filter.APP_FILTERS != null) {
					System.out.println("Loaded " + Filter.APP_FILTERS.length + " filter(s)");
				} else {
					System.out.println("Filters couldn't be loaded because the arraylist is null");
				}
			} catch (Exception e) {
				System.out.println("Error : " + e.getMessage());
			}
		};
	}

	@Bean
	public CommandLineRunner loadGenres(RestTemplate restTemplate) {
		System.out.println("Loading genres...");
		return args -> {
			try {
				System.out.println("Fetching genres from " + baseApiDockerUrl + "/genres");
				Genre.APP_GENRES = restTemplate.getForObject(
						baseApiDockerUrl + "/genres",
						Genre[].class);
				if(Genre.APP_GENRES != null) {
					System.out.println("Loaded " + Genre.APP_GENRES.length + " genre(s)");
				} else {
					System.out.println("Genres couldn't be loaded because the arraylist is null");
				}
			} catch (Exception e) {
				System.out.println("Error : " + e.getMessage());
			}
		};
	}
}
