package com.fennecwave.fwweb.controllers;

import com.fennecwave.fwweb.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Value("${DOCKER-API-URL}")
    private String baseApiUrl;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        ResponseEntity<String> responseEntity = new RestTemplate().postForEntity(baseApiUrl+"/users/login", User.builder().username(s).build(), String.class);
        String password = responseEntity.getBody();
        return org.springframework.security.core.userdetails.User.builder()
                .username(s)
                .password(password)
                .roles("USER")
                .build();
    }

}
