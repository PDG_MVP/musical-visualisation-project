package com.fennecwave.fwweb.controllers;

import com.fennecwave.fwweb.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@Controller
@SessionAttributes("loggedInUsername")
public class LoginController {

    @Value("${DOCKER-API-URL}")
    private String baseApiUrl;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private AuthenticationProvider authProvider;

    //========== LOGIN ============

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    // Login
    @PostMapping({"/login"})
    public String login(
            @RequestParam(name="username") String username,
            @RequestParam(name="password") String password,
            Model model) {
        User user = User.builder().username(username).password(password).build();
        try {
            String savedPassword = new RestTemplate().postForObject(baseApiUrl + "/users/login", user, String.class);
            if(encoder.matches(password, savedPassword))
            {
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
                Authentication auth = authProvider.authenticate(token);
                SecurityContext sc = SecurityContextHolder.getContext();
                sc.setAuthentication(auth);
                model.addAttribute("loggedInUsername", username);
                return "redirect:/";
            }
            else
            {
                model.addAttribute("error", "Passwords don't match");
            }
        }
        catch (HttpClientErrorException e) {
            model.addAttribute("error", "Unknown username");
        }
        return "login";
    }

    //========== REGISTER ============
    @GetMapping("/register")
    public String register() {
        return "register";
    }

    @PostMapping("/register")
    public String register(@RequestParam(name="username") String username,
                           @RequestParam(name="password") String password,
                           @RequestParam(name="confirmPassword") String confirmPassword,
                           Model model) {
        if(username.isEmpty()){
            model.addAttribute("error", "No username");
            return "register";
        }
        if(!password.equals(confirmPassword)){
            model.addAttribute("error", "Passwords don't match");
            return "register";
        }

        User user = User.builder().username(username).password(password).build();
        user.setPassword(encoder.encode(user.getPassword()));

        if(new RestTemplate().postForObject(baseApiUrl + "/users/register", user, Boolean.class)) {
            return "redirect:/login";
        }
        else {
            model.addAttribute("error", "User already registered");
            return "register";
        }
    }

}

//temp R&D stuff, please don't cleanup

            /*ObjectMapper objectMapper = new ObjectMapper();
        try {
            String student = new ObjectMapper().readValue(errorMsg, String.class);
        } catch (JsonProcessingException jsonProcessingException) {
            jsonProcessingException.printStackTrace();
        }

        try {

            JSONArray array = new JSONArray(errorMsg);
            String errorBodyJSON = array.getString(4);
            errorMsg = errorBodyJSON;
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }

       JSONParser parser = new JSONParser(errorMsg);
        try {
            JSONObject json = (JSONObject) parser.parse();
            errorMsg = json.get("message").toString();
        } catch (ParseException | JSONException parseException) {
            parseException.printStackTrace();
        }*/
