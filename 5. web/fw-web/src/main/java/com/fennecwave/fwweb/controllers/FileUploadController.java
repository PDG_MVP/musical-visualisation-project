package com.fennecwave.fwweb.controllers;

import com.fennecwave.fwweb.models.Filter;
import com.fennecwave.fwweb.models.Genre;
import com.fennecwave.fwweb.models.Visual;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class FileUploadController {
    @Value("${DOCKER-API-URL}")
    private String baseApiDockerUrl;

    @Value("${DOCKER-API-URL-EXT}")
    private String baseApiExternalUrl;

    public FileUploadController() {
    }

    @GetMapping("/upload")
    public String uploadFile(Model model, RedirectAttributes redirectAttributes) throws IOException {
        // Check filters are loaded
        if (Filter.APP_FILTERS == null || Filter.APP_FILTERS.length == 0) {
            model.addAttribute("message", "The filters could not be loaded");
            return "error";
        }

        // Check genres are loaded
        if (Genre.APP_GENRES == null ||Genre.APP_GENRES.length == 0) {
            model.addAttribute("message", "The genres could not be loaded");
            return "error";
        }

        // Get data from previous upload (if present)
        Visual uploadedVisual = (Visual) model.getAttribute("visual");
        String previousFilename = (String) model.getAttribute("previousFilename");
        Long previousFilter = (Long) model.getAttribute("previousFilter");
        // TODO: previousGenres

        if(uploadedVisual != null) {
            System.out.println("Visual generated : " + uploadedVisual.getVisualimg());
        }

        // First opening
        model.addAttribute("genres", Genre.APP_GENRES);
        model.addAttribute("filters", Filter.APP_FILTERS);
        model.addAttribute("basicFilterId", Filter.APP_FILTERS[0].getId());

        // Audio file uploaded
        model.addAttribute("visual", uploadedVisual);
        model.addAttribute("previousFilename", previousFilename);
        model.addAttribute("previousFilter", previousFilter);

        return "upload";
    }

    @PostMapping("/upload")
    public String handleFileUpload (
            @RequestParam(value = "file", required = false) MultipartFile audioFile,
            @RequestParam(value = "previousFilename", required = false) String previousFilename,
            @RequestParam("filterId") Long filterId,
            @RequestParam("title") String title,
            @RequestParam("subtitle") String subtitle,
            RedirectAttributes redirectAttributes,
            HttpSession session, Model model) throws IOException {

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

        final String filename;
        if(audioFile != null) {
            System.out.println("Generating a basic visual from file");
            System.out.println("File : " + audioFile.getOriginalFilename());
            System.out.println("===========================");
            filename = audioFile.getOriginalFilename();
            try{
                ByteArrayResource contentsAsResource = new ByteArrayResource(audioFile.getBytes()){
                    @Override
                    public String getFilename(){
                        return filename;
                    }
                };

                map.add("file", contentsAsResource);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Get the metadata of the file
            try {
                // TODO
            } catch (Exception e) {
                model.addAttribute("message", e.getMessage());
                return "Error";
            }

        } else {
            System.out.println("Generating a basic visual from already uploaded file");
            System.out.println("File : " + previousFilename);
            System.out.println("===========================");
            filename = previousFilename;
        }

        map.add("name", filename);
        map.add("filename", filename);
        map.add("filterId", filterId);
        map.add("title", title);
        map.add("subtitle", subtitle);
        // TODO : Genres

        Visual visualGenerated = new RestTemplate().postForObject(
                baseApiDockerUrl + "/upload", map, Visual.class);

        // Get the generated visual
        if(visualGenerated == null) {
            model.addAttribute("message", "The file could not be loaded, it needs to be .wav or .mp3");
            return "Error";
        }
        System.out.println(visualGenerated.getAudioSrcFilename());
        redirectAttributes.addFlashAttribute("visual", visualGenerated);
        redirectAttributes.addFlashAttribute("previousFilename", visualGenerated.getAudioSrcFilename());
        redirectAttributes.addFlashAttribute("previousFilter", filterId);

        return "redirect:/upload";
    }

    @PostMapping("/save")
    public String saveVisual(@RequestParam("idVisual") Long idVisual,
                             @RequestParam("title") String title,
                             @RequestParam("subtitle") String subtitle,
                             @RequestParam("length") String length,
                             @RequestParam("genres") String genres,
                             @RequestParam("pathServed") String pathServed) {

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

        map.add("idVisual", idVisual);
        map.add("title", title);
        map.add("subtitle", subtitle);
        map.add("pathServed", pathServed);
        map.add("length", length);
        map.add("genres", genres);
        map.add("author", getCurrentUsername());

        new RestTemplate().postForObject(baseApiDockerUrl + "/upload/save", map, void.class);
        return "redirect:/profile?tab=Uploads";
    }

    /**
     * Get the username of the current logged in user
     * @return username
     */
    private String getCurrentUsername() {
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return principal.getUsername();
    }
}
