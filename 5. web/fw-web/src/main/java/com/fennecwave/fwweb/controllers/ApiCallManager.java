package com.fennecwave.fwweb.controllers;

import com.fennecwave.fwweb.models.User;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

public class ApiCallManager {

    @ExceptionHandler(HttpClientErrorException.class)
    public String handleError(HttpClientErrorException e, Model model, final HttpServletRequest request) {
        String errorMsg = retrieveErrorMessage(e);
        model.addAttribute("error",  errorMsg);

        return  request.getRequestURI();
    }

    //TODO find cleaner solution
    private String retrieveErrorMessage(HttpClientErrorException e){
        String message = e.getResponseBodyAsString(); //Convert to JSON and get "message"?
        int start, end;
        start =  message.lastIndexOf(":", message.lastIndexOf(",")-1) + 2;
        end = message.lastIndexOf(",") -1;
        message = message.substring(start, end);
        return  message;
    }
}
