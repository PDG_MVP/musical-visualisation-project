package com.fennecwave.fwweb.controllers;

import com.fennecwave.fwweb.models.Visual;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class TrendingController {

    @Value("${DOCKER-API-URL}")
    private String baseApiDockerUrl;

    @GetMapping("/trending")
    public String visuals(Model model) {
        model.addAttribute("files", getVisualsFromApi());
        return "trending";
    }

    private List<Visual> getVisualsFromApi() {
        final String uri = baseApiDockerUrl + "/visuals/trends";

        ResponseEntity<List<Visual>> visualResponse = new RestTemplate().exchange(uri, HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {});

        return visualResponse.getBody();
    }

}
