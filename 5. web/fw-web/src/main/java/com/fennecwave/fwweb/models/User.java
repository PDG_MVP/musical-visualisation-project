package com.fennecwave.fwweb.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;


@AllArgsConstructor
@NoArgsConstructor
@Data // toString, equals, hashCode, getters and setters.
@Builder(toBuilder = true)
public class User {

    private String username;
    private String password;

}
