package com.fennecwave.fwweb.controllers;

import com.fennecwave.fwweb.models.Visual;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Controller
public class IndexController {

    @Value("${DOCKER-API-URL}")
    private String baseApiDockerUrl;

    @GetMapping("/")
    public String visuals(Model model) {

        System.out.println("Loading all visuals by date...");

        // Get all visuals
        List<Visual> visualByDate = getVisualsFromApi();

        // Sort by reverse date (newest first)
        Collections.sort(visualByDate, (o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate()));

        System.out.println("===================");
        for (Visual v : visualByDate) {
            System.out.println(v.getTitle() + " has " + v.getLikes().size() + " like(s)");
        }
        System.out.println("===================");

        model.addAttribute("files", visualByDate);

        return "index";
    }

    /**
     * Get all the visuals from the API
     * @return : list of all visuals
     */
    private List<Visual> getVisualsFromApi() {
        Visual[] visuals = new RestTemplate().getForObject(
                baseApiDockerUrl + "/visuals",
                Visual[].class);

        return visuals == null ? new ArrayList<>() : Arrays.asList(visuals);
    }

}
