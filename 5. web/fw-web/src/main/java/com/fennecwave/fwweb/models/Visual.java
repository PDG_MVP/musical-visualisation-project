package com.fennecwave.fwweb.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Visual {
    private Long id_visual;

    private String title;
    private String subtitle;
    private String visualimg;
    private String audioSrcFilename;

    private Date creationDate;
    private Date modificationDate;
    private String length;
    private String author;

    private String pathServed;

    // Ids of users who likes the visual
    private List<UserVisualLike> likes = new ArrayList<>();

    public Visual() {
    }

    public String getAudioSrcFilename() {
        return audioSrcFilename;
    }

    public void setAudioSrcFilename(String audioSrcFilename) {
        this.audioSrcFilename = audioSrcFilename;
    }

    public String getPathServed() {
        return pathServed;
    }

    public Long getId_visual() {
        return id_visual;
    }

    public void setId_visual(Long idvisual) {
        this.id_visual = idvisual;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getVisualimg() {
        return visualimg;
    }

    public void setVisualimg(String visualimg) {
        this.visualimg = visualimg;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public List<UserVisualLike> getLikes() {
        return likes;
    }

    public void setLikes(List<UserVisualLike> likes) {
        this.likes = likes;
    }

    public List<String> getUsernameLikes() {
        List<String> result = new ArrayList<>();

        for(UserVisualLike like : likes)
            result.add(like.getUsername());

        return result;
    }
}
