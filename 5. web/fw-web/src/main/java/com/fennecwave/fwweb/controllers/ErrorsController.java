package com.fennecwave.fwweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorsController {

    @GetMapping("/error")
    public String errors(Model model, String message) {
        model.addAttribute("message", message);
        return "error";
    }

}