package com.fennecwave.fwweb.controllers;

import com.fennecwave.fwweb.models.Visual;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Controller
public class ProfileController {

    @Value("${DOCKER-API-URL}")
    private String baseApiDockerUrl;

    @GetMapping("/profile")
    public String profile(Model model, @RequestParam(required = false) String tab) {

        if(tab == null || tab.isEmpty())
            tab = "Notifications";

        switch(tab) {
            case "Notifications":
                model.addAttribute("favedVisuals", getFavouritedVisualsFromApi());
                break;
            case "Favourites":
                model.addAttribute("favVisuals", getFavouritesVisualsFromApi());
                break;
            case "Uploads":
                model.addAttribute("authoredVisuals", getAuthoredVisualsFromApi());
                break;
        }

        model.addAttribute("tab", tab);
        return "profile";
    }

    /**
     * Get all the favourites visuals from the API
     * @return : list of all favourites visuals
     */
    private List<Visual> getFavouritedVisualsFromApi() {
        return getVisualList("favourited");
    }

    /**
     * Get all the favourites visuals from the API
     * @return : list of all favourites visuals
     */
    private List<Visual> getFavouritesVisualsFromApi() {
        return getVisualList("favourites");
    }

    /**
     * Get all the favourites visuals from the API
     * @return : list of all favourites visuals
     */
    private List<Visual> getAuthoredVisualsFromApi() {
        return getVisualList("author");
    }

    private List<Visual> getVisualList(String endpoint) {
        // Get username of current session holder
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.getUsername();

        Visual[] visuals = new RestTemplate().getForObject(
                baseApiDockerUrl + "visuals/" + endpoint + "/" + username,
                Visual[].class);

        return visuals == null ? new ArrayList<>() : Arrays.asList(visuals);
    }
}
