package com.fennecwave.fwweb.controllers;

import com.fennecwave.fwweb.models.Genre;
import com.fennecwave.fwweb.models.Visual;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class GenresController {
    @Value("${DOCKER-API-URL}")
    private String baseApiDockerUrl;

    @GetMapping("/genres")
    public String genres(Model model, @RequestParam(required = false) String label) {
        // Check genres are loaded
        if (Genre.APP_GENRES == null ||Genre.APP_GENRES.length == 0) {
            model.addAttribute("message", "The genres could not be loaded");
            return "error";
        }

        // Select first label if no selection
        if(label == null || label.isEmpty()) {
            label = Genre.APP_GENRES[0].getLabel();
        }

        model.addAttribute("genres", Genre.APP_GENRES);
        model.addAttribute("selectedGenre", label);
        model.addAttribute("files", getVisualsFromApiByGenre(label));

        return "genres";
    }

    private List<Visual> getVisualsFromApiByGenre(String label) {
        final String uri = baseApiDockerUrl + "/genres/" + label;
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<List<Visual>> visualResponse = restTemplate.exchange(uri,
                HttpMethod.GET, null,
                new ParameterizedTypeReference<>(){});

        return visualResponse.getBody();
    }

}

