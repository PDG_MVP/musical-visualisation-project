
/**
 * Open / Close top navigation hamburger
 */
document.addEventListener('DOMContentLoaded', () => {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
        // Add a click event on each of them
        $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {

                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }
});

// Search Feature
let searchButton = document.getElementById("search-button");
let searchInput = document.getElementById("search");

function openSearchInput() {
    searchInput.style.display = "block";
    searchInput.focus();
    searchButton.style.display = "none";
}

function closeSearchInput() {
    if(searchInput.value === "") {
        searchInput.style.display = "none";
        searchButton.style.display = "flex";
    }
}

function filterVisuals() {
    let input = document.getElementById('search');
    let filter = input.value.toLowerCase();
    let nodes = document.getElementsByClassName('music-capsule');

    for (let i = 0; i < nodes.length; i++) {

        const titleText = nodes[i].children[1];
        const subtitleText = nodes[i].children[2];

        if (titleText.innerText.toLowerCase().includes(filter) ||
            subtitleText.innerText.toLowerCase().includes(filter)) {
            subtitleText.innerHTML = getHighlightedText(subtitleText.innerText, filter);
            nodes[i].style.display = "grid";
            titleText.innerHTML = getHighlightedText(titleText.innerText, filter);
            nodes[i].style.display = "grid";
        } else {
            nodes[i].style.display = "none";
        }
    }
}

function getHighlightedText(text, filter) {
    let occIndices = getIndicesOf(filter, text, false);
    let startIndex = 0;
    let newInnerText = "";

    for(let j = 0; j < occIndices.length; ++j) {
        newInnerText += text.substring(startIndex, occIndices[j]) + "<mark>" +
            text.substring(occIndices[j], occIndices[j] + filter.length) + "</mark>";
        startIndex = newInnerText.length - ((j + 1) * "<mark></mark>".length);
    }

    newInnerText += text.substring(startIndex);

    return newInnerText;
}

// https://stackoverflow.com/questions/3410464/how-to-find-indices-of-all-occurrences-of-one-string-in-another-in-javascript
function getIndicesOf(searchStr, str, caseSensitive) {
    let searchStrLen = searchStr.length;
    if (searchStrLen === 0) {
        return [];
    }

    let startIndex = 0, index, indices = [];
    if (!caseSensitive) {
        str = str.toLowerCase();
        searchStr = searchStr.toLowerCase();
    }
    while ((index = str.indexOf(searchStr, startIndex)) > -1) {
        indices.push(index);
        startIndex = index + searchStrLen;
    }
    return indices;
}


function likeVisual(visual_id, username) {

    // Create a request variable and assign a new XMLHttpRequest object to it.
    let requestNewLike = new XMLHttpRequest();
    let params = "visualId=" + visual_id + "&username=" + username;

    // Reload when we get an answer
    requestNewLike.onreadystatechange = function() {
        if (requestNewLike.readyState === XMLHttpRequest.DONE) {
            location.reload();
        }
    }

    // Open a new connection, using the POST request on the URL endpoint
    requestNewLike.open('POST', 'http://localhost:8080/users/like', true);
    requestNewLike.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    // Send request
    requestNewLike.send(params);
    //location.reload();
}

function rotateFullview(angle) {
    document.getElementById('visualFullviewImg').style.transform = 'scale(3) rotate(' + -angle + 'deg)';
}

// Stream Feature
let streamBox;

function openStreamBox() {
    streamBox.style.display = "block";
    streamBox.innerText = "No stream found"
    streamBox.focus();
}

function closeStreamBox() {
    streamBox.style.display = "none";
}

function streamVisual(visual_title, visual_sub, visual_id) {
    let selector = ".stream-box[data-vid=\"" +visual_id+"\"]";
    streamBox = document.querySelectorAll(selector)[0];

    // Create a request variable and assign a new XMLHttpRequest object to it.
    let requestStream = new XMLHttpRequest();
    let params = "/?q=" + visual_title + " " + visual_sub + "&country=CH&limit=1";

    // Reload when we get an answer
    requestStream.onreadystatechange = function() {
        if (requestStream.readyState === XMLHttpRequest.DONE) {
            let returnedData = JSON.parse(requestStream.responseText);
            if (typeof(returnedData.data.tracks[0]) == "undefined"){
                openStreamBox();
            }else{
                let streamURL = returnedData.data.tracks[0].sourceUrl;
                window.open(streamURL);
            }
        }
    }

    // Open a new connection, using the GET request on the URL endpoint
    requestStream.open('GET', 'https://cors-anywhere.herokuapp.com/' + 'https://songwhip.com/api' +params, true);
    requestStream.setRequestHeader('Content-type', 'application/json');

    // Send request
    requestStream.send();
}