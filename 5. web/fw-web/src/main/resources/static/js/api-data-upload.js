
var mainTitle = document.getElementById('upload-title');
var genreMapSelection = new Map();
var genresSelected = [];
var filterSelected;

/**
 * Choose a file to upload : opens file explorer
 */
function openFileExplorer() {
    // Simulate click on the file input to open file explorer
    document.getElementById('fileInput').click();

    // Update the title
    mainTitle.innerHTML = "Select a .wav or .mp3 file to upload...";
}

/**
 * Select a new filter and reloads the display
 * @param filter : selected filter
 */
function selectFilter(filter) {

    // Change filter
    filterSelected = filter;
    document.getElementById(filterSelected).classList.add("genre-selected");
    document.getElementById('filterInput').value = filterSelected;

    uploadVisualFromFile()
}

function selectGenre(genre) {
    const newValue = !genreMapSelection.get(genre);
    genreMapSelection.set(genre, newValue);
    if(newValue) {
        document.getElementById(genre).classList.add("genre-selected");
        // Add the genre to the list
        genresSelected.push(genre);
    } else {
        document.getElementById(genre).classList.remove("genre-selected");
        // Remove the genre from the list
        const index = genresSelected.indexOf(genre);
        if (index !== -1) {
            genresSelected.splice(index, 1);
        }
    }
}

/**
 * Upload visual to be generated
 */
function uploadVisual() {

    // Change title
    mainTitle.innerHTML = "Generating your visual...";

    // Loading animation
    document.getElementById('loader1').style.display = "block";
    document.getElementById('loader2').style.display = "block";
    document.getElementById('loader3').style.display = "block";

    // Change thumbnail
    document.getElementById('background').src = "/imgs/ic_uploaded.png";

    // Show cancel button and hide open file explorer button
    document.getElementById('openFileButton').style.display = "none";
    document.getElementById('cancelButton').style.display = "block";

    // Submit the form
    document.getElementById('dataForm').submit();
}

/**
 * Regenerate a new visual from the same file as the initial one
 * (filter changes)
 */
function uploadVisualFromFile() {

    // Loading animation
    document.getElementById('refreshLoader').style.display = "block";
    //document.getElementById('refreshIcon').style.display = "none";

    // Submit the form
    document.getElementById('dataForm').submit();
}

/**
 * Opens editor for visual metadata
 */
function openMetadataEditor() {

    // Retrieve old metadata
    const title = document.getElementById('visualTitle').innerText;
    const subtitle = document.getElementById('visualSubtitle').innerText;

    // Hide metadata and buttons
    document.getElementById('metadata').style.display = "none";
    document.getElementById('editButton').style.display = "none";

    // Show editor
    document.getElementById('editorSubtitle').value = subtitle;
    document.getElementById('editorTitle').value = title;
    document.getElementById('metadataEditor').style.display = "block";
}

/**
 * Saves the metadata into the form
 */
function saveMetadata() {

    // Retrieve new metadata
    const title = document.getElementById('editorTitle').value;
    const subtitle = document.getElementById('editorSubtitle').value;

    // Saves metadata to the data form and update the display
    document.getElementById('visualSubtitle').innerText = subtitle;
    document.getElementById('visualTitle').innerText = title;
    document.getElementById('titleInput').value = title;
    document.getElementById('subtitleInput').value = subtitle;

    // Close the editor
    closeMetadataEditor()
}

/**
 * Closes the metadata editor
 */
function closeMetadataEditor() {
    // Hide editor
    document.getElementById('metadataEditor').style.display = "none";

    // Show metadata and buttons
    document.getElementById('metadata').style.display = "block";
    document.getElementById('editButton').style.display = "block";
}

let dropZone = document.getElementById('dropZone');

if(dropZone !== null) {

    // Get file data on drop
    dropZone.addEventListener('drop', function(e) {
        e.stopPropagation();
        e.preventDefault();
        // Set the data and start uploading
        document.getElementById('background').style.filter = 'invert(0%)';
        document.getElementById('fileInput').files = e.dataTransfer.files;
        uploadVisual();
    });

    // Highlight
    dropZone.addEventListener('dragenter', function(e) {
        document.getElementById('background').style.filter = 'invert(100%)';
    });
    dropZone.addEventListener('dragleave', function(e) {
        document.getElementById('background').style.filter = 'invert(0%)';
    });
    dropZone.addEventListener('dragover', function(e) {
        e.preventDefault();
    });
}

function save() {
    let f = document.getElementById("saveForm");

    // Create the editable input in case it changed
    let titleInput = document.createElement("input");
    titleInput.type = "hidden";
    titleInput.name = "title";
    titleInput.value = document.getElementById("visualTitle").innerText;
    f.appendChild(titleInput);

    let subtitleInput = document.createElement("input");
    subtitleInput.type = "hidden";
    subtitleInput.name = "subtitle";
    subtitleInput.value = document.getElementById("visualSubtitle").innerText;
    f.appendChild(subtitleInput);

    let genresInput = document.createElement("input");
    genresInput.type = "hidden";
    genresInput.name = "genres";
    let formattedGenres = "";
    for(let i = 0; i < genresSelected.length; i++) {
        formattedGenres += genresSelected[i];
        if(i < genresSelected.length - 1)
            formattedGenres += ",";
    }
    genresInput.value = formattedGenres;
    f.appendChild(genresInput);

    f.submit();
}
