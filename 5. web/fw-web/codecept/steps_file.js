// in this file you can append custom step methods to 'I' object
const baseUrl = "http://localhost:8081"
const registerURL = baseUrl + "/register";
const loginURL = baseUrl + "/login";

const registerBtn = locate(".button").withText("Register");
const loginBtn = locate(".button").withText("Login");

const usernameField = locate("#form-input-username");
const pwdField = locate("#form-input-password");
const confPwdField = locate("#form-input-confirmPwd");

//Upload
const filename = "rock.wav";
const pathToFile = "/files/" + filename;

const uploadUrl = baseUrl + "/upload";
const browseButton = locate("#fileInput");

const titleField = locate("#editorTitle")
const subtitleField = locate("#editorSubtitle")
const editBtn = locate("#editButton")
const saveEditBtn = locate("#saveEditButton")

//Search
const searchBtn = locate("#search-button")
const searchBar = locate("#search")

module.exports = function() {
  return actor({

    // Useful steps
    login: function(username, password) {
      this.amOnPage(loginURL)
      this.fillField(usernameField, username);
      this.fillField(pwdField, password);
      this.click(loginBtn);
    },

    register: function(username, password) {
      this.amOnPage(registerURL)
      this.fillField(usernameField, username);
      this.fillField(pwdField, password);
      this.fillField(confPwdField, password)
      this.click(registerBtn);
    },

    prepareUpload: function(title, subtitle){
      this.amOnPage(uploadUrl);
      this.attachFile(browseButton, pathToFile);
      this.click(editBtn);
      this.fillField(titleField, title);
      this.fillField(subtitleField, subtitle);
      this.click(saveEditBtn);
    },

    search: function(title, subtitle, title2, subtitle2){
      this.click(searchBtn);

      //Search by title
      this.fillField(searchBar, title);
      this.see(title);
      this.see(subtitle);
      this.dontSee(title2);
      this.dontSee(subtitle2);

      //Search by subtitle
      this.fillField(searchBar, title2);
      this.see(title2);
      this.see(subtitle2);
      this.dontSee(title);
      this.dontSee(subtitle);
    }
  });
}
