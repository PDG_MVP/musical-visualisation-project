Feature("registration");

const baseUrl = "http://localhost:8081"
const registerURL = baseUrl + "/register";
const loginURL = baseUrl + "/login";

const uniqueId = new Date().getTime();
const uniqueUsername1 = "1registration_test1-" + uniqueId;
const uniqueUsername2 = "1registration_test2-" + uniqueId;
const pwd = "passWord123";

const mismatchPwdMsg = "Passwords don't match";
const usernameTakenMsg= "User already registered";

const registerBtn = locate(".button").withText("Register");

const usernameField = locate("#form-input-username");
const pwdField = locate("#form-input-password");
const confPwdField = locate("#form-input-confirmPwd");


Scenario("Test valid registration redirects to login", (I) => {
    I.register(uniqueUsername1, pwd)
    I.seeInCurrentUrl(loginURL);
});

Scenario("Test invalid registration : mismatched passwords", (I) => {
    I.amOnPage(registerURL);

    I.fillField(usernameField, uniqueUsername2);
    I.fillField(pwdField, pwd);
    I.fillField(confPwdField, pwd + "nope")
    I.click(registerBtn);
    I.seeInCurrentUrl(registerURL);
    I.see(mismatchPwdMsg);
});


Scenario("Test invalid registration : missing field", (I) => {
    I.amOnPage(registerURL);
    I.fillField(usernameField, "");
    I.fillField(pwdField, pwd);
    I.fillField(confPwdField, pwd);
    I.click(registerBtn);
    I.seeInCurrentUrl(registerURL);
});

Scenario("Test invalid registration : username already exists", (I) => {
    I.amOnPage(registerURL);
    I.register(uniqueUsername1, pwd + "different")
    I.seeInCurrentUrl(registerURL);
    I.see(usernameTakenMsg);
});
