Feature("interactions");

const baseUrl = "http://localhost:8081"
const trendingURL = baseUrl + '/trending';

const uniqueId = new Date().getTime();
const uniqueUsername = "6interactions_test-" + uniqueId;
const pwd = "passWord123";

const uploadButton = locate("#uploadBtn");

const title = "How to Leave Your Body"
const subtitle = "John Murphy"

const title2 = '@n2oqhdn8$!!""$`'
const subtitle2 = 'shouldntFindStream'

const streamUrl = 'https://open.spotify.com'

const capsule = locate('.music-capsule').withDescendant('#song-name').withText(title)
const streamBtn = locate('#stream-button').inside(capsule)
const likeBtn = locate('#like').inside(capsule);
const totalLikes = locate('#totalLikes').inside(capsule);

const capsule2 = locate('.music-capsule').withDescendant('#song-name').withText(title2)
const streamBtn2 = locate('#stream-button').inside(capsule2)
const noStream = 'No stream found'

const firstCapsuleTrending = locate('.music-capsules-grid > div:first-child > div:first-child')
const firstCapsuleTrendingLikes = locate('#totalLikes').inside(firstCapsuleTrending);
const lastCapsuleTrending = locate('.music-capsules-grid > div:last-child > div:first-child')
const lastCapsuleTrendingLikes = locate('#totalLikes').inside(lastCapsuleTrending);



Scenario("Upload to prepare test", (I) => {
    I.register(uniqueUsername, pwd);
    I.login(uniqueUsername, pwd);
    I.see("Logout");

    //Upload 1
    I.prepareUpload(title, subtitle);
    I.click(uploadButton);

    //Upload 2
    I.prepareUpload(title2, subtitle2);
    I.click(uploadButton);
});

Scenario("Test like authentified", (I) => {
    I.login(uniqueUsername, pwd);
    I.see("Logout");

    I.amOnPage(baseUrl);

    I.see(title);
    I.see(subtitle);

    I.seeElement(capsule);
    I.seeElement(totalLikes);

    I.seeElement(totalLikes.withText('0 like'));
    I.click(likeBtn);
    I.seeElement(totalLikes.withText('1 like'));
    I.click(likeBtn);

    //Test toggle
    I.seeElement(totalLikes.withText('0 like'));
    I.click(likeBtn);
    I.seeElement(totalLikes.withText('1 like'));
});

Scenario("Test 2nd like authentified", (I) => {
    const uniqueUsername2 = uniqueUsername + '_bis';
    I.register(uniqueUsername2, pwd)
    I.login(uniqueUsername2, pwd);
    I.see("Logout");

    I.amOnPage(baseUrl);

    I.seeElement(capsule);
    I.seeElement(totalLikes);

    I.seeElement(totalLikes.withText('1 like'));
    I.click(likeBtn);
    I.seeElement(totalLikes.withText('2 likes'));
});

Scenario("Test trending anonymous", (I) => {
    I.amOnPage(trendingURL);
    I.see('Login');

    I.seeElement(firstCapsuleTrendingLikes.withText('2 likes'));
    I.seeElement(lastCapsuleTrendingLikes.withText('0 like'));
});

Scenario("Test anonymous can see totalLikes but can't like", (I) => {
    I.amOnPage(baseUrl);
    I.see('Login');

    I.seeElement(capsule);
    I.seeElement(totalLikes);

    I.seeElement(totalLikes.withText('2 likes'));
    I.dontSeeElement(likeBtn);
});

Scenario("Test stream anonymous", (I) => {
    I.amOnPage(baseUrl);

    I.seeElement(capsule);
    I.click(streamBtn);

    I.wait(10);

    I.switchToNextTab();
    I.seeInCurrentUrl(streamUrl);
    I.see(title);
    I.see(subtitle);
});

Scenario("Test stream not found anonymous", (I) => {
    I.amOnPage(baseUrl);

    I.see(title2);
    I.see(subtitle2);

    I.seeElement(capsule2);
    I.click(streamBtn2);

    I.see(noStream);
});

