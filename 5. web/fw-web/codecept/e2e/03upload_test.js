Feature("upload");

const baseUrl = "http://localhost:8081"
const redirectUrl = baseUrl + "/profile?tab=Uploads";

const uniqueId = new Date().getTime();
const uniqueUsername = "3upload_test-" + uniqueId;
const pwd = "passWord123";

const uploadButton = locate("#uploadBtn");
const downloadButton = locate("#download-button")

const title = "SpottieOttieDopaliscious"
const subtitle = "OutKast"

const filterStr = 'JungleNeon'
const filter = locate('.filter-upload-item').withText(filterStr).inside('.filter-item-container')

Scenario("Test anonymous upload and download with filter selection and metadata edition", (I) => {
    I.prepareUpload(title, subtitle);
    I.click(filter);

    I.dontSeeElement(uploadButton);

    I.handleDownloads();
    I.click(downloadButton);

    I.amInPath('output/downloads');
    I.seeFileNameMatching('.png');
});

Scenario("Test upload authentified with metadata edition, filter selection and save", (I) => {
    I.register(uniqueUsername, pwd);
    I.login(uniqueUsername, pwd);

    I.see("Logout");

    I.prepareUpload(title, subtitle);
    I.click(filter);
    I.click(uploadButton);

    I.seeInCurrentUrl(redirectUrl);
    I.see(title);
    I.see(subtitle);
});

Scenario("Test anonymous can see uploads", (I) => {
    I.amOnPage(baseUrl);
    I.see('Login');

    I.see(title);
    I.see(subtitle);
});