Feature("genre");

const baseUrl = "http://localhost:8081"

const uniqueId = new Date().getTime();
const uniqueUsername = "4genre_test-" + uniqueId;
const pwd = "passWord123";

const uploadButton = locate("#uploadBtn");

const genreLabel = 'Rock'
const genreBtn = locate('#' + genreLabel)
const genreBaseURL = baseUrl + '/genres'
const genreURL = genreBaseURL + '?label=' + genreLabel;

const genreLabel2 = 'Animals'
const genreBtn2 = locate('#' + genreLabel2)
const genreURL2 = genreBaseURL + '?label=' + genreLabel2;

const genreLabel3 = 'Acid'
const genreBtn3 = locate('#' + genreLabel3)

const title = "Time"
const subtitle = "Pink Floyd"

Scenario("Test anonymous can't select genre on upload'", (I) => {
    I.prepareUpload(title, subtitle);
    I.dontSeeElement(genreBtn);
});

Scenario("Test valid upload authentified with genre selection", (I) => {
    I.register(uniqueUsername, pwd);
    I.login(uniqueUsername, pwd);

    I.see("Logout");

    I.prepareUpload(title, subtitle);

    I.click(genreBtn);
    I.click(genreBtn3);

    //Test toggle off
    I.click(genreBtn2);
    I.click(genreBtn2);

    I.click(uploadButton);

    I.click('Genres');
    I.seeInCurrentUrl(genreBaseURL);
    I.click(genreLabel);
    I.seeInCurrentUrl(genreURL);
    I.see(title);
    I.see(subtitle);

    I.click(genreLabel2);
    I.seeInCurrentUrl(genreURL2);
    I.dontSee(title);

    I.click(genreLabel3);
    I.see(title);
    I.see(subtitle);
});

Scenario("Test anonymous can see upload on genre pages", (I) => {
    I.amOnPage(baseUrl);
    I.click('Genres');
    I.seeInCurrentUrl(genreBaseURL);
    I.click(genreLabel);
    I.seeInCurrentUrl(genreURL);
    I.see(title);
    I.see(subtitle);

    I.click(genreLabel3);
    I.see(title);
    I.see(subtitle);
});