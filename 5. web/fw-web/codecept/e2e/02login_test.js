Feature("login");

const baseUrl = "http://localhost:8081"

const uniqueId = new Date().getTime();
const uniqueUsername = "2login_test-" + uniqueId;
const pwd = "passWord123";

const wrongUsername = "wrongusername";
const wrongPwd = "wrongpwd";

const invalidUsernameMsg = "Unknown username";
const invalidPswdMsg = "Passwords don't match";

Scenario("Test valid login", (I) => {
    I.register(uniqueUsername, pwd);
    I.login(uniqueUsername, pwd);
    I.seeInCurrentUrl(baseUrl)

    I.dontSee("Login");
    I.see("Logout");
});

Scenario("Test wrong username login", (I) => {
    I.login(wrongUsername, pwd);
    I.see(invalidUsernameMsg);
});

Scenario("Test wrong password login", (I) => {
    I.login(uniqueUsername, wrongPwd);
    I.see(invalidPswdMsg);
});
