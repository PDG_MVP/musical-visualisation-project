Feature("search");

const baseUrl = "http://localhost:8081"
const trendingURL = baseUrl + '/trending';

const uniqueId = new Date().getTime();
const uniqueUsername = "5search_test-" + uniqueId;
const pwd = "passWord123";

const uploadButton = locate("#uploadBtn");

const genreLabel = 'Rap'
const genreBtn = locate('#' + genreLabel)
const genreBaseURL = baseUrl + '/genres'
const genreURL = genreBaseURL + '?label=' + genreLabel;

const title = "Hustlin'"
const subtitle = "Rick Ross"

const title2 = "Any Infrastructure"
const subtitle2 = "Ed Scissor"

const searchBar = locate("#search")

Scenario("Upload with genre to prepare test", (I) => {
    I.register(uniqueUsername, pwd);
    I.login(uniqueUsername, pwd);
    I.see("Logout");

    //Upload 1
    I.prepareUpload(title, subtitle);
    I.click(genreBtn);
    I.click(uploadButton);

    //Upload 2
    I.prepareUpload(title2, subtitle2);
    I.click(genreBtn);
    I.click(uploadButton);
});

Scenario("Test anonymous can search on 'New' page", (I) => {
    I.amOnPage(baseUrl);
    I.search(title, subtitle, title2, subtitle2);

    I.fillField(searchBar, 'z');
    I.dontSee(title);
    I.dontSee(subtitle);
    I.dontSee(title2);
    I.dontSee(subtitle2);

    //Search by subtitle
    I.fillField(searchBar, 'u');
    I.see(title2);
    I.see(subtitle2);
    I.see(title);
    I.see(subtitle);
});



Scenario("Test anonymous can search on 'Trending' page", (I) => {
    I.amOnPage(trendingURL);
    I.search(title, subtitle, title2, subtitle2);

});

Scenario("Test anonymous can search on 'Genre' page", (I) => {
    I.amOnPage(genreURL);
    I.search(title, subtitle, title2, subtitle2);

});