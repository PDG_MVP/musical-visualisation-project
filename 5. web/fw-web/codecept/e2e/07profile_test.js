Feature("profile");

const baseUrl = "http://localhost:8081"
const profileUrl = baseUrl + '/profile'

const uploads = 'Uploads'
const notifications = 'Notifications'
const favourites = 'Favourites'
const uploadsUrl = profileUrl + '?tab=' + uploads
const notificationsUrl = profileUrl + '?tab=' + notifications
const favouritesUrl = profileUrl + '?tab=' + favourites

const uniqueId = new Date().getTime();
const uniqueUsername = "7profile_test-" + uniqueId;
const uniqueUsername2 =  'fanOf_' + uniqueUsername;
const pwd = "passWord123";

const uploadButton = locate("#uploadBtn");

const title = "Cowboy Builders"
const subtitle = "Mr Key"

const capsule = locate('.music-capsule').withDescendant('#song-name').withText(title)
const likeBtn = locate('#like').inside(capsule);

Scenario("Test 'Uploads' tab", (I) => {
    I.register(uniqueUsername, pwd);
    I.login(uniqueUsername, pwd);
    I.see("Logout");

    I.prepareUpload(title, subtitle);
    I.click(uploadButton);
    I.seeInCurrentUrl(profileUrl);
    I.click(uploads);
    I.seeInCurrentUrl(uploadsUrl);
    I.see(title);
    I.see(subtitle);
});

Scenario("Test new user has empty uploads", (I) => {
    I.register(uniqueUsername2, pwd)
    I.login(uniqueUsername2, pwd);
    I.see("Logout");

    I.amOnPage(uploadsUrl);
    I.dontSeeElement(capsule);
});

Scenario("Test 'Favourites' tab", (I) => {
    I.login(uniqueUsername2, pwd);
    I.see("Logout");

    I.amOnPage(baseUrl);

    I.seeElement(capsule);
    I.click(likeBtn);

    I.amOnPage(favouritesUrl);
    I.seeElement(capsule);
});

Scenario("Test 'Notifications' tab", (I) => {
    I.login(uniqueUsername, pwd);
    I.see("Logout");

    I.click(uniqueUsername);

    I.seeInCurrentUrl(profileUrl);
    I.click(notifications);
    I.seeInCurrentUrl(notificationsUrl);
    I.seeElement(capsule);
    I.see(uniqueUsername2)
});

Scenario("Test unlike capsule in 'Favourites' tab", (I) => {
    I.login(uniqueUsername2, pwd);
    I.see("Logout");

    I.amOnPage(favouritesUrl);
    I.seeElement(capsule);
    I.click(likeBtn);
    I.dontSeeElement(capsule);
});
